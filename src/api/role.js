import request from '../utils/request'
import qs from 'qs'
// 获取组
export function groupPlucks() {
    return  request({
        url: '/admin/groupPlucks'
    })
}

// 添加角色
export function addRoles(data) {
    return  request({
        url: '/admin/addRoles',
        method:'POST',
        data:qs.stringify(data)
    })
}

//获取角色
export function getRoles(page,size) {
    return  request({
        url: `/admin/roles?page=${page}&size=${size}`,
    })
}
//获取角色
export function getRolePlucks() {
    return  request({
        url: '/admin/rolePlucks',
    })
}
//获取角色的所有权限
export function getRolesPermission(data) {
    return  request({
        url: '/admin/rolePermissions',
        method: 'POST',
        data:qs.stringify(data)
    })
}
// 获取用户权限
export function getUserPermissions(admin_id) {
    return  request({
        url: '/admin/userPermissions?admin_id='+admin_id
    })
}
// 根据id获取角色详情
export function getRoleDetails(id) {
    return  request({
        url: `/admin/roleDetails/${id}`
    })
}
// 删除角色
export function delRole(id) {
    return  request({
        url: `/admin/delRole/${id}`,
        method:'POST'
    })
}

//提交修改角色
export function editRoles(data, id = data.id) {
    return  request({
        url: `/admin/editRole/${id}`,
        method:'POST',
        data:qs.stringify(data)
    })
}
