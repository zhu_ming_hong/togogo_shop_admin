import request from '../utils/request'
import qs from 'qs'
export function addMenu(data) {
    return  request({
        url: '/admin/addMenu',
        method:'POST',
        data:qs.stringify(data)
    })
}
// 获取菜单、
export function getMenu(page,size) {
    return  request({
        url: `/admin/menus?page=${page}&size=${size}`,
    })
}
// 获取菜单层级
export function getAllMenus() {
    return  request({
        url: `/admin/allMenus`,
    })
}
// 获取菜单层级
export function getMenuPlucks() {
    return  request({
        url: `/admin/menuPlucks`,
    })
}
// 绑定菜单
export function bindRoleMenu(data) {
    return  request({
        url: '/admin/bindRoleMenu',
        method:'POST',
        data:qs.stringify(data)
    })
}
//获取角色菜单
export function getRoleMenus(id) {
    return  request({
        url: `/admin/roleMenus/${id}`,
    })
}
// 删除菜单
export function delMenu(id) {
    return  request({
        url: `/admin/deleteMenu/${id}`,
        method:'POST'
    })
}
