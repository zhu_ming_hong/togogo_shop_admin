import request from '../utils/request'
 import qs from 'qs'
export function getOrder(data) {
   return  request({
        url: `/admin/order/list`,
        method: 'POST',
        data:qs.stringify(data)
    })
}
export function Recharge(data) {
    return  request({
        url: '/admin/index',
        data:qs.stringify(data)
    })
}
export function getRecharge(data) {
    return  request({
        url: `/admin/recharge/list?order_status=${data.order_status}&page=${data.page}&size=${data.size}`,
    })
}
export function getPaging(data) {
  return  request({
      url: `/admin/recharge/list?page=${data.page}&size=${data.size}`,
  })
}
// 获取详情
export function getOrderDetail(id) {
    return  request({
        url: '/admin/order/orderDetails/'+id,
    })
}

// 筛选搜索
export function getPages(data) {
  return  request({
      url: `/admin/recharge/list?&payment_sn=${data.payment_sn}&phone=${data.phone}&start_time=${data.start_time}&end_time=${data.end_time}&order_status=${data.order_status}`,
      // method: 'POST',
      // data:qs.stringify(data)
  })
}

export function changePages(data) {
  return  request({
      url: `/admin/recharge/list?page=${data.page}&size=${data.size}`
      // method: 'POST',
      // data:qs.stringify(data)
  })
}
