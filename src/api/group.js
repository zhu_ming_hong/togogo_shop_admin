import request from '../utils/request'
import qs from 'qs'
export function addGroup(data) {
    return  request({
        url: '/admin/addGroup',
        method:'POST',
        data:qs.stringify(data)
    })
}


//获取列表组
export function getGroup(page,size) {
    return request({
        url:`admin/groups?page=${page}&size=${size}`
    })
}
// 修改组
export function editGroup(data,id) {
    return request({
        url:`/admin/editGroup/${id}`,
        method: 'POST',
        data:qs.stringify(data)
    })
}
// 删除组
export function delGroup(id) {
    return request({
        url:`/admin/delGroup/${id}`,
        method: 'POST'
    })
}
