import request from '../utils/request'
import qs from 'qs'
// 获取 gold 二级子页的页面数据.
// method: GET ;
// url  gold/list ;
// 参数 page | size .
export function getgoldList(data) {
  return  request({
      url: `/admin/gold/list?page=${data.page}&size=${data.size}`
  })
}

export function getgoldListPages(data) {
  return  request({
      url: `/admin/gold/list?page=${data.page}&size=${data.size}`
  })
}

// 筛选搜索
export function getPages(data) {
  return  request({
      url: `/admin/gold/list?start_time=${data.start_time}&end_time=${data.end_time}&keywords=${data.keywords}&type=${data.type}&page=${data.page}&size=${data.size}`
  })
}