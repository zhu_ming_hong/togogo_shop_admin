import request from '../utils/request'
import qs from 'qs'
export function getIndex() {
    return  request({
        url: '/admin/index'
    })
}
