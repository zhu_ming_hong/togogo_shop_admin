import request from '../utils/request'
import qs from 'qs'
export function getPermissions() {
    return  request({
        url: '/admin/allPermissions'
    })
}
// 添加权限
export function addPermission(data) {
   return request({
       url:'/admin/addPermission',
       method:'POST',
       data:qs.stringify(data)
   })
}
//获取所有权限
export function getPermission() {
    return request({
        url:'/admin/permissions'
    })
}
// 删除权限
export function delPermission(id) {
    return request({
        url:`admin/delPermission/${id}`,
        method:'POST'
    })
}
