import request from '../utils/request';
import qs from 'qs';
export function userLists(data) {
  return  request({
      url: `/admin/examineLoanUser/userList?page=${data.page}&size=${data.size}`,
      method: 'GET',
      // method: 'POST',
      // data: qs.stringify(data)
  })
}

export function userListsPages(data) {
  return  request({
      url: `/admin/examineLoanUser/userList?page=${data.page}&size=${data.size}`,
      method: 'GET',
      // method: 'POST',
      // data: qs.stringify(data)
  })
}

export function selectLists(data){
    return  request({
      url: `/admin/examineLoanUser/userList?user_id=${data.user_id}&name=${data.name}&phone=${data.phone}&status=${data.status}`,
      data: qs.stringify(data)
  })
}

export function getUserStatus(id,status){
  return request({
    url: `/admin/examineLoanUser/${id}`,
    method: 'POST',
    data: qs.stringify({status})
  })
}
