import request from '../utils/request'
import qs from 'qs'

export function getuserFinance(data) {
  return  request({
      url: `/admin/finance/loan?page=${data.page}&size=${data.size}&keywords=${data.keywords}`,
      method: 'GET'
  })
}

export function getUserDetail(data) {
  return  request({
      url: `/admin/financeLoan/interestLogs/${data.id}&page=${data.page}&size=${data.size}&principal=${data.principal}`,
      method: 'GET',
      data: qs.stringify(data)
  })
}

export function getUserDetailPages(data) {
  return  request({
      url: `/admin/financeLoan/interestLogs/page=${data.page}&size=${data.size}`,
      method: 'GET'
  })
}

