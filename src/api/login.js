import request from '../utils/request'
import qs from 'qs'
export function login(data) {
   return  request({
        url: '/admin/login',
        method: 'post',
        data:qs.stringify(data)
    })
}
