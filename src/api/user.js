import request from '../utils/request'
import qs from 'qs'
export function getUser(data) {
    return  request({
        url: '/admin/user/list',
        method: 'POST',
        data: qs.stringify(data)
    })
}
//添加管理员

export function addUser(data) {
    return  request({
        url: '/admin/addAdmin',
        method: 'POST',
        data: qs.stringify(data)
    })
}
//修改管理员

export function editUser(data,id = data.id) {
    return  request({
        url: `/admin/editUser/${id}`,
        method: 'POST',
        data: qs.stringify(data)
    })
}
// 获取管理员列表

export function getAdmins(page,size) {
    return  request({
        url: `/admin/admins?page=${page}&size=${size}`,
    })
}

/**
 * 获取管理员详情
 */

export function getDetail(id) {
    return  request({
        url: `/admin/userDetails/${id}`,
    })
}

/**
 * 删除管理员
 */

export function delUser(id) {
    return  request({
        url: `/admin/delUser/${id}`,
        method: 'POST'
    })
}

export function getPages(data) {
  return  request({
      url: `/admin/user/list?keywords=${data.keywords}&start_time=${data.start_time}&end_time=${data.end_time}`,
      method: 'POST',
      data: qs.stringify(data)
  })
}