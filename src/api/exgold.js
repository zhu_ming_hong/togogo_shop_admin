import request from '../utils/request';
import qs from 'qs';

// http://xx.com/cash/list

export function getCaseList(data) {
  return  request({
      url: `/admin/cash/list?page=${data.page}&size=${data.size}`,
      data: qs.stringify(data)
  })
}

export function selectLists(data) {
  return  request({
      url: `/admin/cash/list?keywords=${data.keywords}&start_time=${data.start_time}&end_time=${data.end_time}&status=${data.status}`,
      data: qs.stringify(data)
  })
}

export function getNewList(id,status) {
  return request({
      url: `/admin/gold/withdrawalCashExamine/${id}`,
      method: 'GET',
      // method: 'POST',
      data: qs.stringify({status})
  })
}
