import request from '../utils/request'
import qs from 'qs'
// 获取组
export function addShop(data) {
    return  request({
        url: '/admin/addCommodity',
        method: 'POST',
        data: qs.stringify(data)
    })
}
// 获取列表
export function getShop(data) {
    return  request({
        url: `/admin/commodities?page=${data.page}&size=${data.size}`,
    })
}
//删除商品
export function delShop(id) {
    return request({
        url:`/admin/commodities/delete/${id}`
    })
}
//获取商品详情
export function getShopDetail(id) {
    return request({
        url:`/admin/commodities/details/${id}`
    })
}
export function editSubmit(data) {
return request({
    url:`/admin/commodities/update/${data.id}`,
    method: 'POST',
    data:qs.stringify(data)
})
}
// 删除分类
export function deleteAttrKey(id,editId) {
   return request({
       url:`/admin/deleteAttrKey/${id}?commodity_id=${editId}`
   })
}
// 删除下级分类
export function deleteAttrVal(id,editId) {
    return request({
        url:`/admin/deleteAttrVal/${id}?commodity_id=${editId}`
    })
}
// 删除表格数据
export function deleteTableVal(id) {
    return request({
        url:`/admin/deleteSku/${id}`
    })
}
// 删除价格
export function deletePrice(id) {
    return request({
        url:`/admin/deletePrice/${id}`
    })
}
// 获取分类
export function getCate() {
    return request({
        url:`/admin/classifyPlucks`
    })
}

// 发送商品名和价格范围
export function sendData(data){
  return request({
    url: `/admin/commodities?keywords=${data.keywords}&min=${data.min}&max=${data.max}`
  })
}