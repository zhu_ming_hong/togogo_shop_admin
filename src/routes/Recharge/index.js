import React from 'react'
import CustomBreadcrumb from '../../components/CustomBreadcrumb/index'
import {Table, Button, Input, DatePicker, Select} from 'antd';
import {getRecharge, getPages, getPaging } from "../../api/order";
import './index.css';

const { Option } = Select;
const { RangePicker } = DatePicker;
const InputGroup = Input.Group;
const columns = [
    // {
    //     title: '序号',
    //     dataIndex: 'name',
    //     width: 50,
    //     align: 'center',
    //     render:(text,record,index)=>(
    //         index + 1
    //     )
    // },
    {
        title: '充值编号',
        dataIndex: 'internal_payment_sn',
        width: 200,
        align: 'center'
    },
    {
        title: '充值人',
        dataIndex: 'address',
        width: 200,
        align: 'center',
        render:(text,record) =>(
          record.sessions.phone
        )
    }
    ,
    {
        title: '充值金额',
        dataIndex: 'total_price',
        width: 200,
        align: 'center'
    },
    {   
        title: '赠送金额',
        dataIndex: 'give_price',
        width: 200,
        align: 'center'
    },
    {
        title: '日期',
        dataIndex: 'created_at',
        width: 200,
        align: 'center'
    },
    {
        title: '账户余额',
        dataIndex: 'sessions[balance]',
        width: 200,
        align: 'center'
    }
];

class Shop extends React.Component {
    state = {
        selectedRowKeys: [], // Check here to configure the default column
        loading: false,
        order_status:0,
        data:[],
        start_time: '',
        end_time: '',
        delivery_type: 0,
        total: 0,
        current: 0
    };

    start = () => {
        this.setState({loading: true});
        // ajax request after empty completing
        setTimeout(() => {
            this.setState({
                selectedRowKeys: [],
                loading: false,
            });
        }, 1000);
    };
    onSelectChange = selectedRowKeys => {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        this.setState({selectedRowKeys});
    };
    // 页面跳转逻辑
    changePage = (page) => {
      this.setState({
        current: page,
      }, () => {
        console.log(this.state.page)
        var data = {
          page: this.state.current,
          size: 10
        }
        getPaging(data).then((res) => {
          const list = res.data.data
          if (list) {
              this.setState({
                  data: list,
                  total: res.data.total
              })
          }
        })
      })
    }
    componentDidMount() {
        let data = {
         order_status : this.state.order_status,
         page: 1,
         size: 10
        }
        getRecharge(data).then((res) => {
            if(res.data.data) {
                this.setState({
                    data:res.data.data,
                    total:res.data.total
                })
            }
        })

        const inputDefault =  document.querySelectorAll('.ant-calendar-range-picker-input');
        var arr = ['充值日期', '充值日期'];
        for(let index = 0; index < inputDefault.length; index++){
          inputDefault[index].placeholder = arr[index];
        }
        let ulDemo = document.querySelectorAll('.ant-table-pagination');
        console.log(ulDemo)
    }
    getDeliveryType=(value)=>{
      if(value === '0'){
        this.setState({
          delivery_type: ''
        })
      }else if(value === '1'){
        this.setState({
          delivery_type: 1
        })
      }else if(value === '2'){
        this.setState({
          delivery_type: 2
        })
      }
    }
    getInputValue() {
      let firstInput = document.querySelectorAll('input')[0].value; // keywords
      let secondInput = document.querySelectorAll('input')[1].value; //input_01
      let start_time = this.state.start_time;
      let end_time = this.state.end_time;
      let order_status = this.state.delivery_type;
      console.log(order_status);
      let data = {
        phone: firstInput,
        payment_sn: secondInput,
        start_time: start_time,
        end_time: end_time,
        order_status: order_status
      }
      // 搜索框筛选搜索，当筛选内容为空时，右下角分页器隐藏
      getPages(data).then((res) => {
        const list = res.data.data
        console.log('返回数据')
        console.log(list);
        if(list.length === 0){
          this.setState({
            total: -1
          })
          let jump = document.getElementById('jumpVessel');
          jump.style.display = 'none';
        }else{
          this.setState({
            total: list.total
          })
        }
        this.setState({
          data: list
        })
      })
    }
    // 当搜索框值为 '',右下角分页器显示
    textNone=()=>{
      let firstInput = document.querySelectorAll('input')[0].value;
      var data = {
        order_status: this.state.order_status,
        page: 1,
        size: 10
      }
      if(firstInput === '' ){
        getRecharge(data).then((res) => {
          if(res.data.data) {
              this.setState({
                  data:res.data.data,
                  total:res.data.total
              })
          }
        })
      }    
    }
    onPressEnter=()=>{
      let enterInput = document.getElementById('onPressEnter').value;
      const data = {
        page: enterInput,
        size: 10
      }
      getRecharge(data).then((res) => {
        if(res.data.data) {
            this.setState({
                data:res.data.data,
                current: res.data.current_page
            })
        }
      })
    }
    render() {
        const {loading, selectedRowKeys} = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };
        const hasSelected = selectedRowKeys.length > 0;
        return (
            <div>
                <CustomBreadcrumb arr={['商品']}/>
                <div style={{marginBottom: 16}}>
                    <InputGroup compact style={{ floatL: 'left' }}>
                      <Button type="primary" onClick={this.start} disabled={!hasSelected} loading={loading}>
                          删除
                      </Button>
                      <Input placeholder="请输入手机号" onChange={this.textNone} style={{ width: 120 }}/>
                      <Input placeholder="请输入充值编号"  style={{ width: 150, borderRight: '1px solid rgb(217,217,217)', borderTopRightRadius: '4px', borderBottomRightRadius: '4px' }}/>
                        <span className="changeDate">请输入日期:</span>
                      <RangePicker onChange={(value)=>{
                        // 获取搜索框属性改变时，对应的两个时间
                        if(value != false){
                          var timer = value;
                          let start_time = this.$moment(timer[0]._d).format('YYYY-MM-DD');
                          let end_time = this.$moment(timer[1]._d).format('YYYY-MM-DD');
                          console.log(start_time);
                          console.log(end_time);
                          this.setState({
                            start_time: start_time,
                            end_time: end_time
                          })
                          console.log(this.state)
                          }
                      }}/>
                      <Select defaultValue="0" style={{ width: 100 }} onChange={this.getDeliveryType}>
                        <Option value="0">- 全部 -</Option>
                        <Option value="1">- 未支付 -</Option>
                        <Option value="2">- 已支付 -</Option>
                      </Select>
                      <Button type="primary" shape="circle" icon="search" onClick={() => this.getInputValue()}/>
                    </InputGroup>

                </div>
                <Table rowSelection={rowSelection} rowKey="id" columns={columns} dataSource={this.state.data} pagination={{ total: this.state.total,onChange: this.changePage, current: this.state.current }}/>
                <span id="jumpVessel" style={{ fontSize: '14px', color: 'rgb(37,37,37)', float:'right', margin: '0px 18px 20px 0px' }}>
                    前往第&nbsp;&nbsp;<Input id="onPressEnter" onPressEnter={this.onPressEnter} style={{ width: '32px', height: '32px', padding: '4px', textAlign: 'center' }}></Input>&nbsp;&nbsp;页
                </span>
            </div>
        );
    }
}

export default Shop
