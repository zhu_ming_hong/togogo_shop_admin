import React from 'react'
import CustomBreadcrumb from '../../components/CustomBreadcrumb/index'
import {Table, Button, Input, DatePicker, Skeleton } from 'antd';
import { getUser, getPages } from "../../api/user";
import './index.css';

const { RangePicker } = DatePicker;
const InputGroup = Input.Group;
const columns = [
    {
        title: '序号',
        width: 100,
        align: 'center',
        render: (text, record, index)=>{
           return (index+1)
        },
    },
    {
        title: '用户名',
        dataIndex: 'open_id',
        width: 220,
        align: 'center'
    },
    {
        title: '联系手机',
        dataIndex: 'phone',
        width: 100,
        align: 'center'
    },
    {
        title: '购买次数',
        dataIndex: 'orders_count',
        width: 100,
        align: 'center'
    },
    {
        title: '购买金额',
        dataIndex: 'consume_price',
        width: 100,
        align: 'center',
        render: (text,record,index)=>{
          if(text == 0){
            return `${text}`
          }else{
            return `${text} 元`
          }
        }
    },
    {
        title: '余额',
        dataIndex: 'balance',
        width: 100,
        align: 'center',
        render: (text,record,index)=>{
          if(text == 0){
            return `${text}`
          }else{
            return `${text} 元`
          }
        }
    },
    {
        title: '添加日期',
        dataIndex: 'created_at',
        width: 200,
        align: 'center'
    },

];

class Index extends React.Component {
    state = {
        selectedRowKeys: [], // Check here to configure the default column
        loading: false,
        page:1,
        size:10,
        data:[],
        value: '',
        timer: Array,
        start_time: '',
        end_time: '',
        total: 0,
        current: 0
    };

    start = () => {
        this.setState({loading: true});
        setTimeout(() => {
            this.setState({
                selectedRowKeys: [],
                loading: false,
            });
        }, 1000);
    };

    onSelectChange = selectedRowKeys => {
        this.setState({selectedRowKeys});
    };

    // 页面跳转逻辑
    changePage = (page) => {
      console.log(page);
      this.setState({
        current: page,
      }, () => {
        var data = {
          page: this.state.current,
          size: 10
        }
        getUser(data).then((res) => {
          const list = res.data.data
          if (list) {
              this.setState({
                  data: list,
                  total: res.data.total
              })
          }
        })
      })
    }

    // 当搜索框值为 '',右下角分页器显示
    textNone=()=>{
      let firstInput = document.querySelectorAll('input')[0].value;
      if(firstInput === ''){
        const data = {
          page:1,
          size:10
      }
      getUser(data).then((res) => {
          const list = res.data.data
          if (list) {
              this.setState({
                  data: list,
                  total: res.data.total
              })
          }
      })
      let jump = document.getElementById('jumpVessel');
      jump.style.display = 'block';
    }
    }

    componentDidMount() {
        const data = {
            page:1,
            size:10
        }
        getUser(data).then((res) => {
            const list = res.data.data
            if (list) {
                this.setState({
                    data: list,
                    total: res.data.total
                })
            }
        })
        // 修改框内默认值.
        // 原生 JS 获取对应 class-dom数组, for循环进行修改.
        // 设置搜索框内的 string值
        const inputDefault =  document.querySelectorAll('.ant-calendar-range-picker-input');
        var arr = ['注册日期', '注册日期'];
        for(let index = 0; index < inputDefault.length; index++){
          inputDefault[index].placeholder = arr[index];
        }

        // console.log(moment)
    }

    getInputValue=()=>{
      let firstInput = document.querySelectorAll('input')[0].value; // keywords
      let start_time = this.state.start_time;
      let end_time = this.state.end_time;
      let data = {
        keywords: firstInput,
        start_time: start_time,
        end_time: end_time
      }
      // 搜索框筛选搜索，当筛选内容为空时，右下角分页器隐藏
      getPages(data).then((res) => {
          const list = res.data.data
          if(list.length === 0){
            this.setState({
              total: -1
            })
            let jump = document.getElementById('jumpVessel');
            jump.style.display = 'none';
          }else{
            this.setState({
              total: list.total
            })
          }
          this.setState({
            data: list
          })
      })
    }

    onPressEnter=()=>{
      let enterInput = document.getElementById('onPressEnter').value;
      const data = {
        page: enterInput,
        size: 10
      }
      getUser(data).then((res) => {
        const list = res.data.data
        console.log('user返回数据');
        console.log(res.data);
        if (list) {
            this.setState({
                data: list,
                total: res.data.total,
                current: res.data.current_page
            })
        }
      })
    }

    render() {
        const { loading, selectedRowKeys } = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };
        const hasSelected = selectedRowKeys.length > 0;
        return (
            <div>
                <CustomBreadcrumb arr={['用户列表']}/>
                    {/* Ant_D 内的组合框，必须使用 InputGroup 进行标签组合
                       日期选择器： Rageicker, 想要获取内部的值, 
                       须等待用户选择两个值后，onChange 事件回调获取,
                       处理 onChange内的回调 value可以使用 moment.js
                       来进行指定形式的格式化, 注意引入对应的模块儿。 */}
                    <InputGroup compact className='inputVessel'>
                      <Button type="primary" className='deleteBtn' style={{ display: 'inline-block', marginBottom: '14px' }} onClick={this.start} disabled={!hasSelected} loading={loading} >
                          删除
                      </Button>
                      {/* 文本搜索框 */}
                      <Input placeholder="请输入电话号码" onChange={this.textNone} style={{ width: 160 , borderRight: '1px solid rgb(217,217,217)', borderTopRightRadius: '4px', borderBottomRightRadius: '4px'}} />
                      {/* 日期搜索框 */}
                      <span className="changeDate">请输入日期:</span>
                      <RangePicker onChange={(value)=>{
                        // 获取搜索框属性改变时，对应的两个时间
                        if(value != false){
                        var timer = value;
                        let start_time = this.$moment(timer[0]._d).format('YYYY-MM-DD');
                        let end_time = this.$moment(timer[1]._d).format('YYYY-MM-DD');
                        this.setState({
                          start_time: start_time,
                          end_time: end_time
                        })
                        }
                      }} /> 
                      {/* 搜索按钮_请求 */}
                      <Button type="primary" shape="circle" icon="search" onClick={() => this.getInputValue()}/>
                    </InputGroup>  
                  <Table bordered rowSelection={rowSelection}  rowKey="id" columns={columns} dataSource={this.state.data} pagination={{ total: this.state.total,onChange: this.changePage, current: this.state.current }} />  
                <span id="jumpVessel" style={{ fontSize: '14px', color: 'rgb(37,37,37)', float:'right', margin: '0px 18px 20px 0px' }}>
                    前往第&nbsp;&nbsp;<Input id="onPressEnter" onPressEnter={this.onPressEnter} style={{ width: '32px', height: '32px', padding: '4px', textAlign: 'center' }}></Input>&nbsp;&nbsp;页
                </span>
                
            </div>
        );
    }
}

export default Index
