import React from 'react' // React 库
import CustomBreadcrumb from '../../components/CustomBreadcrumb/index'// 传值进入该文件，分条件渲染 Ant_D组件 
import {Table, Input, Button, message, Modal, Select} from 'antd'; // Ant_D 按需加载
import { userLists, getUserStatus,userListsPages, selectLists } from "../../api/oncredit";// http 请求 api
import './index.css';
const InputGroup = Input.Group;
const {Option} = Select;

// 创建 Examine 模块
class Examine extends React.Component {
  state = {
    list: [],
    conditionPass: '通过',
    conditionReject: '拒绝',
    loading: false, // 默认 loading取消
    corrent_page: 0, // 保存当前页面
    passModal: false, // 通过
    rejectModal: false, // 拒绝
    per_page: 0,
    select_status: 0,
    current: 0,
    total: 0
  }
    componentDidMount() {
      this._getuserlists()
    }
    // 闭包调用 Promise
    _getuserlists(){
      let data = {
        page: 1,
        size: 10
      };
      userLists(data).then((res) => {
          if(res.code < 400) {
            const list = res.data.data;
            console.log(list);
            if(list.length > 0) {
                this.setState({
                    list: list,
                    per_page: res.data.per_page,
                    total: res.data.total
                })
                console.log(res.data.per_page)
            }
        }
      })
    }
    // 按钮操作 调用嵌套
    _getUserStatus(user_id,status){
    return  getUserStatus(user_id,status).then((res)=>{
        if(res.code == 200){
            var that = this;
            setTimeout(function(){
              that.state.loading = false;
            },2000); 
        }
      })
      // getUserStatus(user_id,status);
    }
    // 调用弹窗
    _success = (changes) => {
      console.log('弹窗执行');
      message.config({
        top: 64
      });
      message.success(`${changes}`,[2]);
      // message.destroy();
    };

    // 点击通过_取消关闭会话框
    conceal_OK=()=>{
      this.setState({
        passModal: false
      })
    }
    // 点击拒绝_取消关闭会话框
    conceal_No=()=>{
      this.setState({
        rejectModal: false
      })
    }
    // 页面跳转逻辑
    changePage = (page) => {
      this.setState({
        current: page,
      }, () => {
        var data = {
          page: this.state.current,
          size: 10
        }
        userListsPages(data).then((res) => {
          if(res.code < 400) {
            const list = res.data.data;
            console.log(res);
            if(list) {
                this.setState({
                    list: list,
                    total: res.data.total
                })
            }
        }
      })
      })
    }
    // 获取页面选取状态
    getStatus=(value)=>{
      if(value === '0'){
        this.setState({
          select_status: ''
        })
      }else if(value === '1'){
        this.setState({
          select_status: 0
        })
      }else if(value === '2'){
        this.setState({
          select_status: 1
        })
      }else if(value === '3'){
        this.setState({
          select_status: 2
        })
      }
    }
    
    // 发送筛选请求
    getInputValue=()=>{
      let userInput = document.querySelectorAll('input')[0].value;
      let nameInput = document.querySelectorAll('input')[1].value;
      let phoneInput = document.querySelectorAll('input')[2].value;
      let datas = {
        user_id: userInput,
        name: nameInput,
        phone: phoneInput,
        status: this.state.select_status
      }

      selectLists(datas).then((res) => {
        const list = res.data.data
        console.log(list);
        if(list) {
            this.setState({
                list: list,
                total: res.data.total
            })
            let jump = document.getElementById('jumpVessel');
            jump.style.display = 'none';
        }else{
          this.setState({
            total: -1
          })
        }
      })
    }

    onPressEnter=()=>{
      let enterInput = document.getElementById('onPressEnter').value;
      const data = {
        page: enterInput,
        size: 10
      }
      userLists(data).then((res) => {
          const list = res.data.data;
          console.log(list);
          if(list.length > 0) {
              this.setState({
                  list: list,
                  total: res.data.total,
                  current: res.data.current_page
              })
              console.log(res.data.per_page)
          }
    })
    }
    textNone=()=>{
      let data = {
        page: 1,
        size: 10
      };
      userLists(data).then((res) => {
          if(res.code < 400) {
            const list = res.data.data;
            console.log(list);
            if(list.length > 0) {
                this.setState({
                    list: list,
                    per_page: res.data.per_page,
                    total: res.data.total
                })
                console.log(res.data.per_page)
            }
        }
      })
    }
    render() {
      // 自定义导航节点类别
        const columns = [
          {   
              rowKey: '1',
              title: '用户编码',
              dataIndex: 'user_id',
              width: 200,
              align: 'center'
          },
          {   
              rowKey: '2',
              title: '用户姓名',
              dataIndex: 'name',
              width: 100,
              align: 'center'
          },
          {   
              rowKey: '3',
              title: '电话号码',
              dataIndex: 'phone',
              width: 200,
              align: 'center'
          },
          {   
              rowKey: '4',
              title: '身份证号',
              dataIndex: 'id_card',
              width: 200,
              align: 'center'
          },
          {   
              rowKey: '5',
              title: '地址',
              dataIndex: 'address',
              width: 200,
              align: 'center'
          },
          {   
              rowKey: '6',
              title: '状态',
              // key: 'status',
              dataIndex: 'status',
              width: 200,
              align: 'center',
              render: status => {
                if(status === 0){
                  return(
                    <div style={{ color: '#666' }}>未处理</div>
                  )
                }else if(status === 1){
                  return(
                    <div style={{ color: 'rgb(54,189,261)' }}>已通过</div>
                  )
                }else if(status === 2){
                  return(
                    <div style={{ color: 'rgb(255,77,99)' }}>已拒绝</div>
                  )
                }
              }
          },
          {
            rowKey: '7',
              title: '操作',
              dataIndex: 'status',
              width: 140,
              align: 'center',
              // 这个坑可以记一下，这种渲染模式，通过设置dataIndex:status,
              // render里面取的第一个值就是你要的状态值
              // 通常他的第二个参数可以返回当前对象，你想要传个 id标识什么的就可以直接取到
              render: (status,user) => {
                var user_id = user.id;
                if(status === 0){
                  return(
                  <div>
                    <span style={{ display: 'flex', justifyContent: 'space-between' }}>
                      <Button  onClick={()=>{
                         Modal.confirm({
                          cancelText: '取消',
                          okText: '确定',
                          title: <div>您确定要‘通过’吗</div>,
                          onOk:() =>{
                            // console.log('222')
                            this._getUserStatus(user_id,1);
                            this._getuserlists();
                            this._success('已受理请求');            
                          }
                        })
                      }
                      }>通过</Button>
                      <Button  type="danger"  onClick={()=>{
                         Modal.confirm({
                          cancelText: '取消',
                          okText: '确定',
                          title: <div>您确定要‘拒绝’吗</div>,
                          onOk:() =>{
                            // console.log('111');
                            this._getUserStatus(user_id,2);
                            this._getuserlists();
                            this._success('已拒绝请求');
                          }
                        })
                      }
                      }>拒绝</Button>
                    </span>
                  </div>
                  )
                }else if(status === 1){
                  return(
                    <div style={{ color: 'rgb(54,189,261)' }}>已通过</div>
                  )
                }else if(status === 2){
                  return(
                    <div style={{ color: 'rgb(255,77,99)' }}>已拒绝</div>
                  )
                }
              }
          }
        ];

        return(<div>
                <CustomBreadcrumb arr={['订单']}/>
                <InputGroup compact style={{ marginBottom: '16px' }}>
                  <Input placeholder="请输入用户编码" onChange={this.textNone} style={{ width: 140}}  />
                  <Input placeholder="请输入用户姓名"  style={{ width: 140}} />
                  <Input placeholder="请输入电话号码"  style={{ width: 140}} />
                  <Select defaultValue="0" style={{ width: 100 }} onChange={this.getStatus}>
                    <Option value="0">- 全部 -</Option>
                    <Option value="1">- 未处理 -</Option>
                    <Option value="2">- 已通过 -</Option>
                    <Option value="3">- 已拒绝 -</Option>
                  </Select>
                  <Button type="primary" shape="circle" icon="search"
                  onClick={() => this.getInputValue()} />
                </InputGroup>
                <Table rowKey="id" columns={columns} loading={this.state.loading} dataSource={this.state.list} pagination={{
                  per_page: this.state.per_page,
                  onChange: this.changePage,
                  total: this.state.total
                }}/>
                <span id="jumpVessel" style={{ fontSize: '14px', color: 'rgb(37,37,37)', float:'right', margin: '0px 18px 20px 0px' }}>
                    {/* 前往第&nbsp;&nbsp;<Input id="onPressEnter" onPressEnter={this.onPressEnter} style={{ width: '32px', height: '32px', padding: '4px', textAlign: 'center' }}></Input>&nbsp;&nbsp;页 */}
                </span>
              </div>)
        }
}

export default Examine