import React from 'react'
import CustomBreadcrumb from '../../components/CustomBreadcrumb/index'// 传值进入该文件，分条件渲染 Ant_D组件 
import {Table, message, Button, Input, DatePicker, Select, Modal, Icon} from 'antd';
import { getCaseList, getNewList, selectLists } from "../../api/exgold";// http 请求 api
const { Option } = Select;
const { RangePicker } = DatePicker;
const InputGroup = Input.Group;

class ExamineGold extends React.Component{
  // _state
  state = {
    list: [],
    start_time: '',
    end_time: '',
    status: '',
    conditionPass: '通过',
    conditionReject: '拒绝',
    loading: false, // 默认 loading取消
    corrent_page: 0, // 保存当前页面
    passModal: false, // 通过
    rejectModal: false, // 拒绝
    per_page: 0,
    total: 0
  }
  // _初次渲染
  componentDidMount(){
    this._getCaseList();
  }
  // _初次渲染请求页面数据
  _getCaseList(){
    let data = {
      page: 1,
      size: 10
    }
    getCaseList(data).then((res) => {
        if(res.code < 400) {
          const list = res.data.data;
          console.log(list)
          if(list.length > 0) {
              this.setState({
                  list: list,
                  per_page: res.data.per_page,
                  total: res.data.total
              })
          }
          console.log(res.data.per_page)
        }
    })
  }
  // 按钮操作 调用嵌套
  _getNewList(id,status){
    return getNewList(id,status).then((res)=>{
      if(res.code == 200){
        var that = this;
        setTimeout(function(){
          that.state.loading = false;
        },2000); 
      }
    })
  }
  // 获取选择框的状态
  getStatus=(value)=>{
    if(value === '0'){
      this.setState({
        status: ''
      })
    }else if(value === '1'){
      this.setState({
        status: 0
      })
    }else if(value === '2'){
      this.setState({
        status: 1
      })
    }else if(value === '3'){
      this.setState({
        status: 2
      })
    }
  }
  // 搜索框清空
  textNone=()=>{
    let firstInput = document.querySelectorAll('input')[0].value;
    if(firstInput === ''){
      const data = {
        page: 1,
        size: 10
      }
      getCaseList(data).then((res) => {
        const list = res.data.data
          if(list) {
              this.setState({
                  list: list,
                  total: res.data.total
              })
          }
    })
    let jump = document.getElementById('jumpVessel');
    jump.style.display = 'block';
    }

  }
  // 页面跳转逻辑
  changePage = (page) => {
    this.setState({
      current: page,
    }, () => {
      var data = {
        page: this.state.current,
        size: 10
      }
      getCaseList(data).then((res) => {
        if(res.code < 400) {
          const list = res.data.data;
          if(list.length > 0) {
              this.setState({
                  list: list,
                  per_page: res.data.per_page
              })
          }
        }
      })
    })
  }
  // 调用弹窗
  _success = (changes) => {
    // console.log('弹窗执行');
    message.config({
      top: 64
    });
    message.success(`${changes}`,[2]);
    // message.destroy();
  };

  // 点击通过_取消关闭会话框
  conceal_OK=()=>{
    this.setState({
      passModal: false
    })
  }
  // 点击拒绝_取消关闭会话框
  conceal_No=()=>{
    this.setState({
      rejectModal: false
    })
  }

  // 筛选搜索
  getInputValue=()=>{
    let userInput = document.querySelectorAll('input')[0].value; // keywords
    let datas = {
      keywords: userInput,
      start_time: this.state.start_time, // 开始时间
      end_time: this.state.end_time, // 结束时间
      status: this.state.status // 选取状态
    }
    
    // 用户筛选请求
    selectLists(datas).then((res) => {
      const list = res.data.data
        if(list.length === 0){
          this.setState({
            total: -1
          })
          let jump = document.getElementById('jumpVessel');
          jump.style.display = 'none';
        }else{
          this.setState({
            total: list.total
          })
        }
        this.setState({
          list: list
        })
    })
  }

   onPressEnter=()=>{
      let enterInput = document.getElementById('onPressEnter').value;
      const data = {
        page: enterInput,
        size: 10
      }
      getCaseList(data).then((res) => {
        if(res.code < 400) {
          const list = res.data.data;
          console.log(list)
          if(list.length > 0) {
              this.setState({
                  list: list,
                  per_page: res.data.per_page,
                  current: res.data.current_page
              })
          }
          console.log(res.data.per_page)
        }
    })
    }

  // 渲染数据
  render(){

    const columns = [
      {   
          rowKey: '1',
          title: 'id',
          dataIndex: 'id',
          width: 100,
          align: 'center'
      },
      {   
          rowKey: '2',
          title: '用户 id',
          dataIndex: 'user_id',
          width: 200,
          align: 'center'
      },
      
      {   
          rowKey: '3',
          title: '金额',
          dataIndex: 'price',
          width: 200,
          align: 'center'
      },
      {   
          rowKey: '4',
          title: '添加时间',
          dataIndex: 'created_at',
          width: 300,
          align: 'center'
      },
      {   
          rowKey: '5',
          title: '状态',
          dataIndex: 'status',
          key: 'id',
          width: 300,
          align: 'center',
          render: status => {
            if(status === 0){
              return(
                <div style={{ color: '#666' }}>未处理</div>
              )
            }else if(status === 1){
              return(
                <div style={{ color: 'rgb(54,189,261)' }}>已打款</div>
              )
            }else if(status === 2){
              return(
                <div style={{ color: 'rgb(255,77,99)' }}>已拒绝</div>
              )
            }
          }
      },
      {   
          rowKey: '6',
          title: '操作',
          dataIndex: 'status',
          // key: 'id',
          width: 100,
          align: 'center',
          render: (status,user) => {
            // console.log(user);
            var id = user.id;
            // console.log(id);
            if(status === 0){
              return(
              <div>
                <span style={{ display: 'flex', justifyContent: 'space-between' }}>
                  <Button  onClick={()=>{
                    // console.log('2222')
                    Modal.confirm({
                      cancelText: '取消',
                      okText: '确定',
                      title: <div>您确定要‘通过’吗</div>,
                      onOk:() =>{
                        // console.log('222')
                        this._getNewList(id,1);
                        this._getCaseList();
                        this._success('已受理请求')               
                      }
                    })
                  }
                  }>通过</Button>
                  <Button  type="danger"  onClick={()=>{
                    Modal.confirm({
                      cancelText: '取消',
                      okText: '确定',
                      title: <div>您确定要‘拒绝’吗</div>,
                      onOk:() =>{
                        this._getNewList(id,2);
                        this._getCaseList();
                        this._success('已拒绝请求')
                      }
                    })
                  }
                  }>拒绝</Button>
                </span>
              </div>
              )
            }else if(status === 1){
              return(
                <div style={{ color: 'rgb(54,189,261)' }}>已通过</div>
              )
            }else if(status === 2){
              return(
                <div style={{ color: 'rgb(255,77,99)' }}>已拒绝</div>
              )
            }
          }
      }
    ]

    return(<div>
            <CustomBreadcrumb arr={['订单']}/>
            <InputGroup compact style={{ marginBottom: '16px' }}>
            {/* <Button type="primary">
              删除
            </Button> */}
            <Input placeholder="请输入用户 id" onChange={this.textNone}   style={{ width: 140 , borderRight: '1px solid rgb(217,217,217)', borderTopRightRadius: '4px', borderBottomRightRadius: '4px'}} />
                      {/* 日期搜索框 */}
                      <span style={{ padding: '4px 11px 4px 64px' }}>请输入日期:</span>
                      <RangePicker onChange={(value)=>{
                        // 获取搜索框属性改变时，对应的两个时间
                        if(value != false){
                          var timer = value;
                          let start_time = this.$moment(timer[0]._d).format('YYYY-MM-DD');
                          let end_time = this.$moment(timer[1]._d).format('YYYY-MM-DD');
                          this.setState({
                            start_time: start_time,
                            end_time: end_time
                          })
                        }
                      }}/> 
                      {/* 下拉选取 - 订单类型*/}
                      <Select defaultValue="0" style={{ width: 120 }} onChange={this.getStatus}>
                        <Option value="0">- 全部 -</Option>
                        <Option value="1">- 未处理 -</Option>
                        <Option value="2">- 已打款 -</Option>
                        <Option value="3">- 已拒绝 -</Option>
                      </Select>
                      {/* 搜索按钮 */}
                      <Button type="primary" shape="circle" icon="search" onClick={() => this.getInputValue()} />
            </InputGroup>
            <Table rowKey="id" columns={columns} dataSource={this.state.list} pagination={{
              per_page: this.state.per_page,
              onChange: this.changePage,
              total: this.state.total, current: this.state.current
            }}/>
            <span id="jumpVessel" style={{ fontSize: '14px', color: 'rgb(37,37,37)', float:'right', margin: '0px 18px 20px 0px' }}>
                    {/* 前往第&nbsp;&nbsp;<Input id="onPressEnter" onPressEnter={this.onPressEnter} style={{ width: '32px', height: '32px', padding: '4px', textAlign: 'center' }}></Input>&nbsp;&nbsp;页 */}
            </span>
          </div>)
  }
}

export default ExamineGold