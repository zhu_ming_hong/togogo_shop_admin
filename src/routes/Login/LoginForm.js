import React from 'react'  // React
import { randomNum,calculateWidth } from '../../utils/utils' //工具函数 *该导出函数用来计算 CSS宽度样式
import { withRouter } from 'react-router-dom' // withRouter属于 React的高阶组件，其作用: 将一个组件包裹进 Route 里面，然后 react-router的三个对象，history,location,match就会被放进这个组件的 props属性里。举个例子，如果我们做的某个东西不是一个 Router,但是我们需要依靠它去跳转一个页面，比如点击页面的 logo,返回首页，这时候就可以用 withRouter来做。 
import { inject, observer } from 'mobx-react/index' // npm包，用来做存储数据状态，同步渲染
import { Form, Input, Row, Col } from 'antd' // Ant_D Ui组件
import PromptBox from '../../components/PromptBox' // 验证码组件儿  **该组件已被拆除**
import {login} from '../../api/login' // 登录验证 api

@withRouter @inject('appStore') @observer @Form.create()
class LoginForm extends React.Component {
  state = {
    focusItem: -1,  //保存当前聚焦的input
    // code: ''         //验证码
  }
  componentDidMount () {
    // this.createCode()
    document.onkeydown = function(e) {
      if(e.keyCode == 13) {
          e.preventDefault ? e.preventDefault() : (e.returnValue = false);
          return false
      }
    }
  }
  loginSubmit = (e) => {
    e.preventDefault()
    this.setState({
      focusItem: -1
    })
    this.props.form.validateFields((err, values) => {
      login({username: values.username, password : values.password,type:1}).then((res)=>{
        if(res.data) {
          localStorage.setItem("menu",JSON.stringify(res.data.menu))
          const menus = localStorage.getItem('menu')
          console.log(menus)
          res.data.menu = ''
          this.props.appStore.toggleLogin(true, JSON.stringify(res.data))
          const {from} = this.props.location.state || {from: {pathname: '/'}}
          this.props.history.push(from)
        }
      })
    })
  }
    createCode = () => {
    const ctx = this.canvas.getContext('2d')
    const chars = [1, 2, 3, 4, 5, 6, 7, 8, 9, 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    let code = ''
    ctx.clearRect(0, 0, 80, 39)
    for (let i = 0; i < 4; i++) {
      const char = chars[randomNum(0, 57)]
      code += char
      ctx.font = randomNum(20, 24) + 'px SimHei'  //设置字体随机大小
      ctx.fillStyle = '#fb4545'
      ctx.textBaseline = 'middle'
      ctx.shadowOffsetX = randomNum(-3, 3)
      ctx.shadowOffsetY = randomNum(-3, 3)
      ctx.shadowBlur = randomNum(-3, 3)
      ctx.shadowColor = 'rgba(0, 0, 0, 0.2)'
      let x = 80 / 5 * (i + 1)
      let y = 39 / 2
      let deg = randomNum(-25, 25)
      /**设置旋转角度和坐标原点**/
      ctx.translate(x, y)
      ctx.rotate(deg * Math.PI / 180)
      ctx.fillText(char, 0, 0)
      /**恢复旋转角度和坐标原点**/
      ctx.rotate(-deg * Math.PI / 180)
      ctx.translate(-x, -y)
    }
    this.setState({
      code
    })
  }
  render () {
    const {getFieldDecorator, getFieldError} = this.props.form
    const {focusItem, code} = this.state
    return (
      <div className={this.props.className}>
        <img src={require('../../assets/img/LOGO11.png')} style={{ width: '7%',
    height: '12.3%', marginLeft: '8.9%', marginTop: '9.1%', marginBottom: '3%'}}></img>
        <span className='title'>脱狗车宝</span>
        <Form onSubmit={this.loginSubmit}>
          <span className="hintTittle">输入账号</span>
          <Form.Item help={getFieldError('username') &&
          <PromptBox info={getFieldError('username')} width={calculateWidth(getFieldError('username'))}/>}>
            {getFieldDecorator('username', {
              rules: [{required: true, message: '请输入用户名'}]
            })(
              <Input
                onFocus={() => this.setState({focusItem: 0})}
                onBlur={() => this.setState({focusItem: -1})}
                maxLength={16}
                placeholder='用户名'
                addonBefore={<span className='iconfont icon-User' style={focusItem === 0 ? styles.focus : {}}/>}/>
            )}
          </Form.Item>
          <span className="hintTittle hintpass" style={{ marginTop: '2%' }}>输入密码</span>
          <Form.Item help={getFieldError('password') &&
          <PromptBox info={getFieldError('password')} width={calculateWidth(getFieldError('password'))}/>}>
            {getFieldDecorator('password', {
              rules: [{required: true, message: '请输入密码'}]
            })(
              <Input
                onFocus={() => this.setState({focusItem: 1})}
                onBlur={() => this.setState({focusItem: -1})}
                type='password'
                maxLength={16}
                placeholder='密码'
                addonBefore={<span className='iconfont icon-suo1' style={focusItem === 1 ? styles.focus : {}}/>}/>
            )}
          </Form.Item>
          {/* <Form.Item className='btnitem' help={getFieldError('verification') &&
          <PromptBox info={getFieldError('verification')} width={calculateWidth(getFieldError('verification'))}/>}>
            {getFieldDecorator('verification', {
              validateFirst: true,
              rules: [
                {required: true, message: '请输入验证码'},
                {
                  validator: (rule, value, callback) => {
                    if (value.length >= 4 && code.toUpperCase() !== value.toUpperCase()) {
                      callback('验证码错误')
                    }
                    callback()
                  }
                }
              ]
            })(
              <Row>
                <Col span={4}>
                  <Input
                    onFocus={() => this.setState({focusItem: 2})}
                    onBlur={() => this.setState({focusItem: -1})}
                    maxLength={4}
                    placeholder='验证码'
                    addonBefore={<span className='iconfont icon-securityCode-b'
                                       style={focusItem === 2 ? styles.focus : {}}/>}/>
                </Col>
                <Col span={20}>
                  <canvas onClick={this.createCode} width="80" height='39' ref={el => this.canvas = el}/>
                </Col>
              </Row>
            )}
          </Form.Item> */}
            <input className='loginBtn' type="submit" value='登录'/>
        </Form>
      </div>
    )
  }
}

const styles = {
  focus: {
    width: '20px',
    opacity: 1
  }
}

export default LoginForm
