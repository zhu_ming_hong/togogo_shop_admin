import React from 'react'
import CustomBreadcrumb from '../../components/CustomBreadcrumb/index'
import {Table, Button, Input, Menu, Select, DatePicker, Switch} from 'antd'; // 注入下拉选框
import { getOrder } from "../../api/order";
const { Option } = Select;
const { RangePicker } = DatePicker;
const InputGroup = Input.Group;

class Shop extends React.Component {
    state = {
        selectedRowKeys: [], // Check here to configure the default column
        loading: false,
        Parameters:{
            page: 1,
            size: 10
        },
        data:[],
        pay_type: '',
        order_status: '',
        delivery_type: '',
        start_time: '',
        end_time: '',
        delivery_type: '',
        total: 0,
        current: 0
    };

    start = () => {
        this.setState({loading: true});
        // ajax request after empty completing
        setTimeout(() => {
            this.setState({
                selectedRowKeys: [],
                loading: false,
            });
        }, 1000);
    };

    onSelectChange = selectedRowKeys => {
        // console.log('selectedRowKeys changed: ', selectedRowKeys);
        this.setState({selectedRowKeys});
    };
    componentDidMount() {
        const Parameters = {
          page: 1,
          size: 10
        }
        getOrder(Parameters).then((res) => {
            const data = res.data.data
            console.log(res)
           if(data) {
               this.setState({
                   data: data,
                   total: res.data.total
               })
           }
        })

        const inputDefault =  document.querySelectorAll('.ant-calendar-range-picker-input');
        var arr = ['创建日期', '创建日期'];
        for(let index = 0; index < inputDefault.length; index++){
          inputDefault[index].placeholder = arr[index];
        }
    }

    // 获取data值
    // 
    getPayType=(value)=>{
      if(value === '0'){
        this.setState({
          pay_type: ''
        })
      }else if(value === '1'){
        this.setState({
          pay_type: 1
        })
      }else if(value === '2'){
        this.setState({
          pay_type: 2
        })
      }else if(value === '3'){
        this.setState({
          pay_type: 3
        })
      }else if(value === '4'){
        this.setState({
          pay_type: 4
        })
      }else if(value === '5'){
        this.setState({
          pay_type: 5
        })
      }
    }
    getOrderStatus=(value)=>{
      if(value === '0'){
        this.setState({
          order_status: ''
        })
      }else if(value === '1'){
        this.setState({
          order_status: 1
        })
      }else if(value === '2'){
        this.setState({
          order_status: 2
        })
      }else if(value === '3'){
        this.setState({
          order_status: 3
        })
      }else if(value === '4'){
        this.setState({
          order_status: 4
        })
      }else if(value === '5'){
        this.setState({
          order_status: 5
        })
      }
    }
    getDeliveryType=(value)=>{
      // console.log(value);
      if(value === '0'){
        this.setState({
          delivery_type: ''
        })
      }else if(value === '1'){
        this.setState({
          delivery_type: 1
        })
      }else if(value === '2'){
        this.setState({
          delivery_type: 2
        })
      }
    }
    // 页面跳转逻辑
    changePage = (page) => {
      this.setState({
        current: page,
      }, () => {
        var data = {
          page: this.state.current,
          size: 10
        }
        // const Parameters = this.state.Parameters
        getOrder(data).then((res) => {
            const data = res.data.data
           if(data) {
               this.setState({
                   data: data,
                   total: res.data.total
               })
           }
        })
      })
    }
    // 获取输入框的值，发送请求
    getInputValue=()=>{
      let firstInput = document.querySelectorAll('input')[0].value;
      let Parameters = {
          page: 1,
          size: 10,
          pay_type: this.state.pay_type,
          order_status: this.state.order_status,
          keywords: firstInput,
          delivery_type: this.state.delivery_type,
          start_time: this.state.start_time,
          end_time: this.state.end_time
      }

      getOrder(Parameters).then((res) => {
        const list = res.data.data
        if(list.length === 0){
          this.setState({
            total: -1
          })
          let jump = document.getElementById('jumpVessel');
          jump.style.display = 'none';
        }else{
          this.setState({
            total: list.total
          })
        }
        this.setState({
          data: list
        })
    })
      
    }

    textNone=()=>{
      let firstInput = document.querySelectorAll('input')[0].value;
      if(firstInput === ''){
        const data = {
          page:1,
          size:10
      }
      getOrder(data).then((res) => {
        const list = res.data.data
        if(list) {
            this.setState({
                data: list,
                total: res.data.total
            })
        }
    })
    let jump = document.getElementById('jumpVessel');
      jump.style.display = 'block';
    }
  }

  onPressEnter=()=>{
    let enterInput = document.getElementById('onPressEnter').value;
    const data = {
      page: enterInput,
      size: 10
    }
    getOrder(data).then((res) => {
      const data = res.data.data
      console.log(res)
     if(data) {
         this.setState({
             data: data,
             current: res.data.current_page
         })
     }
  })
  }

    render() {
        const columns = [
            {
                rowKey: '1',
                title: '订单编号',
                dataIndex: 'internal_order_sn',
                width: 200,
                align: 'center'
            },
            {
                rowKey: '2',
                title: '创建日期',
                dataIndex: 'created_at',
                width: 200,
                align: 'center'
            },
            {
                rowKey: '3',
                title: '订单总价',
                dataIndex: 'order_total_price',
                width: 200,
                align: 'center',
                render: (text,record,index)=>{
                  if(text == 0){
                    return `${text}`
                  }else{
                    return `${text} 元`
                  }
                }
            },
            {
                rowKey: '4',
                title: '用户名',
                dataIndex: 'contacts',
                width: 200,
                align: 'center'
            },
            {
                rowKey: '5',
                title: '订单类型',
                dataIndex: 'pay_type',
                width: 200,
                align: 'center',
                render: (type)=>{
                    if(type === 1) {
                        return '余额支付'
                    } else if(type === 2) {
                        return '微信支付'
                    } else if(type === 3) {
                        return '现金支付'
                    } else if (type === 4) {
                        return '农商'
                    }else if (type === 5) {
                        return '红包余额'
                    }
                }
            },
            {
                rowKey: '6',
                title: '订单状态',
                dataIndex: 'order_status',
                width: 200,
                align: 'center',
                render: (type)=>{
                    if(type === 1) {
                        return '待付款'
                    } else if(type === 2) {
                        return '待收货'
                    } else if(type === 3) {
                        return '交易成功'
                    } else if (type === 4) {
                        return '订单关闭'
                    }else if (type === 5) {
                        return '异常订单'
                    }
                }
            },
            {
                rowKey: '7',
                title: '操作',
                align: 'center',
                width: 200,
                render: (text, record) => (
                    <span>
                <Button style={{marginRight: 10}} onClick={()=>{
                    this.props.history.push('/orderDetail/'+record.id)
                }}>详情</Button>
                <Button type="danger" >删除</Button>
             </span>
                ),
            },
        ];
        const {loading, selectedRowKeys} = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };
        const hasSelected = selectedRowKeys.length > 0;

        return (
            <div>
                <CustomBreadcrumb arr={['订单']}/>
                <div style={{marginBottom: 16}}>
                    {/* 搜索框组合 */}
                    <InputGroup compact style={{ marginBottom: '16px' }}>
                      <Button type="primary" onClick={this.start} disabled={!hasSelected} loading={loading}>
                          删除
                      </Button>
                      {/* 文本搜索框 */}
                      <Input placeholder="请输入订单号" onChange={this.textNone}   style={{ width: 140 , borderRight: '1px solid rgb(217,217,217)', borderTopRightRadius: '4px', borderBottomRightRadius: '4px'}} />
                      {/* 日期搜索框 */}
                      <span style={{ padding: '4px 11px 4px 64px' }}>请输入日期:</span>
                      <RangePicker onChange={(value)=>{
                        // 获取搜索框属性改变时，对应的两个时间
                        if(value != false){
                          var timer = value;
                          let start_time = this.$moment(timer[0]._d).format('YYYY-MM-DD');
                          let end_time = this.$moment(timer[1]._d).format('YYYY-MM-DD');
                          this.setState({
                            start_time: start_time,
                            end_time: end_time
                          })
                        }
                      }}/> 
                      {/* 下拉选取 - 订单类型*/}
                      <Select defaultValue="0" style={{ width: 120 }} onChange={this.getPayType}>
                        <Option value="0">- 全部 -</Option>
                        <Option value="1">- 余额支付 -</Option>
                        <Option value="2">- 微信支付 -</Option>
                        <Option value="3">- 现金支付 -</Option>
                        <Option value="4">- 农商 -</Option>
                        <Option value="5">- 红包余额 -</Option>
                      </Select>
                      {/* 下拉选取 - 订单状态*/}
                      <Select defaultValue="0" style={{ width: 120 }} onChange={this.getOrderStatus}>
                        <Option value="0">- 全部 -</Option>
                        <Option value="1">- 待付款 -</Option>
                        <Option value="2">- 待收货 -</Option>
                        <Option value="3">- 待评价 -</Option>
                        <Option value="4">- 订单关闭 -</Option>
                        <Option value="5">- 异常订单 -</Option>
                      </Select>
                      {/* 下拉选取 - 配送状态 */}
                      <Select defaultValue="0" style={{ width: 100 }} onChange={this.getDeliveryType}>
                        <Option value="0">- 全部 -</Option>
                        <Option value="1">- 自提 -</Option>
                        <Option value="2">- 物流 -</Option>
                      </Select>
                      {/* 搜索按钮 */}
                      <Button type="primary" shape="circle" icon="search" onClick={() => this.getInputValue()} />
                    </InputGroup>
                </div>
                <Table rowSelection={rowSelection}  rowKey="id" columns={columns} dataSource={this.state.data} pagination={{ total: this.state.total,onChange: this.changePage, current: this.state.current }}/>
                <span id="jumpVessel" style={{ fontSize: '14px', color: 'rgb(37,37,37)', float:'right', margin: '0px 18px 20px 0px' }}>
                    前往第&nbsp;&nbsp;<Input id="onPressEnter" onPressEnter={this.onPressEnter} style={{ width: '32px', height: '32px', padding: '4px', textAlign: 'center' }}></Input>&nbsp;&nbsp;页
                </span>
            </div>
        );
    }
}

export default Shop
