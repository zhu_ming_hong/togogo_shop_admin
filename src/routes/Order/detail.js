import React from 'react'
import {Table, Button, Input, Row, Col, Form} from 'antd';
import {getOrderDetail} from "../../api/order";
import './detail.css'

class Detail extends React.Component {
    state = {
        data: {
            internal_order_sn: '2222',
            internal_payment_sn: '',
            details: []
        },
        status: ["全部", "待付款", "待配送", "已完成"],
        delivery_types:["","到店自取","送货上门"],
        is_freights:["","物流配送","成都本地免费配送"],
        pay_status:["未支付","","已支付"]
    }

    componentDidMount() {
        let id = this.props.match.params.id
        if (id) {
            getOrderDetail(id).then(res => {
                if(res.code < 400) {
                    let data = res.data;
                    this.setState({
                        data
                    })
                }
            })
        }
    }

    render() {
        const columns = [
            {
                title: '商品名称',
                dataIndex: 'commodity_name',
                key: 'commodity_name'
            },
            {
                title: '数量',
                dataIndex: 'number',
                key: 'number'
            },
            {
                title: '属性',
                dataIndex: 'sku_name',
                key: 'sku_name'
            },
            {
                title: '价格',
                dataIndex: 'original_price',
                key: 'original_price'
            },
            {
                title: '活动价格',
                dataIndex: 'activity_price',
                key: 'activity_price'
            }
        ]
        return (
            <Form className='dataGather'>
                <div className="gutter-example">
                  <Row className="fillBox">

                    <Col className="gutter-row" span={8}  md={5}>
                      <div className="gutter-box onlyBack">
                        订单号:&nbsp;&nbsp;&nbsp;{this.state.data.internal_order_sn}
                      </div>
                    </Col>
                    <Col className="gutter-row" span={9}  md={12}>
                      <div className="gutter-box gutter-box-second onlyBack">
                        支付单号:&nbsp;&nbsp;&nbsp;{this.state.data.internal_payment_sn}
                      </div>
                    </Col>
                    <Col className="gutter-row" span={7}>
                      <div className="gutter-box onlyBack">
                        订单状态:&nbsp;&nbsp;&nbsp;{this.state.status[this.state.data.order_status]}
                      </div>
                    </Col>

                  </Row>
                </div>

                <div className="gutter-example">
                  <Row className="fillBox">
                      <Col className="gutter-row" span={8}  sm={5}>
                        <div className="gutter-box  top-left-Radius">
                          下单时间:&nbsp;&nbsp;{this.state.data.created_at}
                        </div>
                      </Col>
                      <Col className="gutter-row" span={9}  sm={12}>
                        <div className="gutter-box gutter-box-second">
                          订单修改时间:&nbsp;&nbsp;{this.state.data.updated_at}
                        </div>
                      </Col>
                      <Col className="gutter-row" span={7}>
                        <div className="gutter-box  top-right-Radius">
                          取货方式:&nbsp;&nbsp;{this.state.delivery_types[this.state.data.delivery_type]}
                        </div>
                      </Col>
                  </Row>
                </div>

                <div className="gutter-example">
                  <Row className="fillBox">
                      <Col className="gutter-row" span={8}  sm={5}>
                        <div className="gutter-box">
                          送货方式:&nbsp;&nbsp;{this.state.is_freights[this.state.data.is_freight]}
                        </div>
                      </Col>
                      <Col className="gutter-row" span={9}  sm={12}>
                        <div className="gutter-box gutter-box-second">
                          配送地址:&nbsp;&nbsp;{this.state.data.full_address}
                        </div>
                      </Col>
                      <Col className="gutter-row" span={7}>
                        <div className="gutter-box">
                          支付状态:&nbsp;&nbsp;{this.state.pay_status[this.state.data.pay_type]}
                        </div>
                      </Col>
                  </Row>
                </div>

                <div className="gutter-example">
                  <Row className="fillBox">
                      <Col className="gutter-row" span={8}  sm={5}>
                        <div className="gutter-box">
                          订单总金额:&nbsp;&nbsp;{this.state.data.order_total_price}
                        </div>
                      </Col>
                      <Col className="gutter-row" span={9}  sm={12}>
                        <div className="gutter-box gutter-box-second">
                          所属门店:&nbsp;&nbsp;{this.state.data.shop_id}
                        </div>
                      </Col>
                      <Col className="gutter-row" span={7}>
                          <div className="gutter-box">&emsp;</div>
                      </Col>
                  </Row>
                </div>

                <div className="gutter-example">
                  <Row className="fillBox">
                      <Col className="gutter-row" span={8}  sm={5}>
                        <div className="gutter-box bottom-left-Radius">
                          联系人:&nbsp;&nbsp;{this.state.data.contacts}
                        </div>
                      </Col>
                      <Col className="gutter-row" span={9}  sm={12}>
                        <div className="gutter-box gutter-box-second">
                          联系电话:&nbsp;&nbsp;{this.state.data.mobile}
                        </div>
                      </Col>
                      <Col className="gutter-row" span={7}>
                        <div className="gutter-box bottom-right-Radius">&emsp;</div>
                      </Col>
                  </Row>
                </div>
                
                <div>
                  <Row className="fillBox">
                    <Table columns={columns} rowKey='id' dataSource={this.state.data.details} pagination={false} style={{ marginTop: '4px',  rowKey: "uid"}}/>
                  </Row>
                </div>
                
                <div>
                  <Row className="fillBox">
                    <Button onClick={()=>{this.props.history.go(-1)}} style={{marginTop: '20px'}}>
                        返回
                    </Button>
                  </Row>
                </div>
            </Form>
        )
    }
}

const DetailFrom = Form.create()(Detail);
export default DetailFrom