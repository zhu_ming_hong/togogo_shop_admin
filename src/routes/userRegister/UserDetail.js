import React from 'react'
import {Table, Button, Icon} from 'antd';
import {getUserDetail, getUserDetailPages} from '../../api/userregister';
// import { withRouter } from 'react-router'
import './UserDetail.css'

class UserDetail extends React.Component {
   state = {
      list: [],
      user_id: 0,
      principal: 0,
      states: null,
      total: 0
   }
   componentDidMount() {
    const principal = this.props.location.state;
    console.log(principal);
    
    this.setState({
      principal: principal
    })
    
    console.log(principal)
  
    window.beforeunload=()=>{
      this.props.history.push('/finance/loan');
    }

   
    // if (data.id) {
        let data = {
          id: this.props.match.params.id,
          page: 1,
          size: 20,
          principal: this.state.principal
        };
        getUserDetail(data).then(res => {
          console.log(res)
            if(res.code < 400) {
                let list = res.data.data;
                this.setState({
                    list: list,
                    total: res.data.total
                })
            }
        })
    // }
  }

  // 页面跳转逻辑
  changePage = (page) => {
    this.setState({
      current: page,
    }, () => {
      var data = {
        page: this.state.current,
        size: 10
      }
      getUserDetailPages(data).then((res) => {
        if(res.code < 400) {
          const list = res.data.data;
          if(list.length > 0) {
              this.setState({
                  list: list,
                  total: res.data.total
              })
          }
        }
      })
    })
  }

  toFather=()=>{
    this.props.history.push('/finance/loan');
  }

   render(){
    const columns = [
      { 
          rowKey: '1',
          title: '利息',
          dataIndex: 'interest',
          width: 200,
          align: 'center',
          render: (text,record,index)=>{
            if(text == 0){
              return `${text}`
            }else{
              return `${text} 元`
            }
          }
      },
      {   
          rowKey: '2',
          title: '产生利息时间',
          dataIndex: 'created_at',
          width: 200,
          align: 'center'
      }
    ]
      return(<div>
                <div style={{ margin: '10px 0 20px 0', fontSize: '14px' }}>
                  <Button type="default" onClick={() => this.toFather()} style={{ marginRight: '10px' }}>
                    <Icon type="left"/>
                  </Button>
                  <span style={{ fontSize: '16px' }}>id :  {this.state.principal[0]}</span>&nbsp;&nbsp;&nbsp;&nbsp;
                  <span style={{ fontSize: '16px' }}>本金 : {this.state.principal[1]} 元</span>
                </div>
                <Table rowKey="id" columns={columns} dataSource={this.state.list} pagination={{
                  per_page: this.state.per_page,
                  onChange: this.changePage,
                  total: this.state.total
                }}/>
            </div>)
   }
}

export default UserDetail