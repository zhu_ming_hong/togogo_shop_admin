import React from 'react';
import CustomBreadcrumb from '../../components/CustomBreadcrumb/index';
import {Table, Button, Input} from 'antd';
import {getuserFinance} from '../../api/userregister';
const { Search } = Input;
const InputGroup = Input.Group;

class UserRegister extends React.Component{
  state = {
    list: [],
    receive_time: '',
    total: 0,
    current: 0
  }
  componentDidMount(){
    this._getuserlist();
  }
  _getuserlist(){
    let data = {
      page: 1,
      size: 10
    }
    getuserFinance(data).then((res)=>{
      if(res.code < 400) {
        const list = res.data.data;
        console.log(list);
        if(list.length > 0) {
            this.setState({
                list: list,
                total: res.data.total
            })
        }else{
          return false
        }
      }
    })
  }
  getInputValue = () =>{
    let firstInput = document.querySelectorAll('input')[0].value;
    let data = {
      keywords: firstInput,
      page: 1,
      size: 10
    }
    getuserFinance(data).then((res)=>{
      if(res.code < 400) {
        const list = res.data.data;
        console.log(list);
        if(list.length > 0) {
            this.setState({
                list: list,
                total: res.data.total
            })
        }else{
          return false
        }
      }
    })
    console.log('11')
  }
  // 页面跳转逻辑
  changePage = (page) => {
    console.log(page);
    this.setState({
      current: page,
    }, () => {
      var data = {
        page: this.state.current,
        size: 10
      }
      getuserFinance(data).then((res) => {
        const list = res.data.data
        if (list) {
            this.setState({
                list: list,
                total: res.data.total
            })
        }
      })
    })
  }
  onPressEnter=()=>{
      let enterInput = document.getElementById('onPressEnter').value;
      const data = {
        page: enterInput,
        size: 10
      }
      getuserFinance(data).then((res)=>{
        if(res.code < 400) {
          const list = res.data.data;
          console.log(list);
          if(list.length > 0) {
              this.setState({
                  list: list,
                  current: res.data.current_page
              })
          }else{
            return false
          }
        }
      })
    }
  render(){
    const columns = [
      {   
        rowKey: '1',
        title: 'id',
        dataIndex: 'id',
        width: 130,
        align: 'center'
      },
      {   
          rowKey: '2',
          title: '用户 id',
          dataIndex: 'user_id',
          width: 160,
          align: 'center'
      },
      { 
          rowKey: '3',
          title: '添加时间',
          dataIndex: 'created_at',
          width: 200,
          align: 'center'
      },
      {   
          rowKey: '4',
          title: '借款金额',
          dataIndex: 'principal',
          width: 160,
          align: 'center',
          render: (text,record,index)=>{
            if(text == 0){
              return `${text}`
            }else{
              return `${text} 元`
            }
          }
      },
      {   
          rowKey: '5',
          title: '当月利息',
          dataIndex: 'interest',
          width: 160,
          align: 'center',
          render: (text,record,index)=>{
            if(text == 0){
              return `${text}`
            }else{
              return `${text} 元`
            }
          }
      },
      {     
          rowKey: '6',
          title: '领取状态',
          dataIndex: 'status',
          width: 160,
          align: 'center',
          render: status => {
            if(status === 0){
              return<div style={{ color: '#666)' }}>未领取</div>
            }else if(status === 1){
              return<div style={{ color: 'rgb(54,189,261)' }}>已领取</div>
            }
          }
      },
      {
          rowKey: '7',
          title: '领取时间',
          dataIndex: 'receive_time',
          width: 180,
          align: 'center',
          render: (text,record,index)=>{
            if(record.receive_time !== null){
              return `${record.receive_time}`
            }else{
              return `未领取`
            }
          }
      },
      {
          rowKey: '8',
          title: '操作',
          dataIndex: 'view_details',
          align: 'center',
          width: 120,
          render: (text, record) => (
            <span>
              <Button style={{marginRight: 10}} onClick={()=>{
                  this.props.history.push({ 
                    pathname: `/financeLoan/interestLogs/${record.id}`,
                    state: [record.id, record.principal]
                      // user_id: record.id,
                      
                    
                  })
              }}>详情</Button>
            </span>
          ),
      }    
    ];
    return(<div>
            <CustomBreadcrumb arr={['菜单列表']}/>
            <InputGroup compact style={{ marginBottom: '16px' }}>
                <Input placeholder="请输入用户id"  style={{ width: 140}}/>
                <Button type="primary" shape="circle" icon="search"
                onClick={() => this.getInputValue()} />
              </InputGroup>
            <Table rowKey="id" columns={columns} dataSource={this.state.list} pagination={{ 
              total: this.state.total,onChange: this.changePage, current: this.state.current
            }}/>
            <span id="jumpVessel" style={{ fontSize: '14px', color: 'rgb(37,37,37)', float:'right', margin: '0px 18px 20px 0px' }}>
                    前往第&nbsp;&nbsp;<Input id="onPressEnter" onPressEnter={this.onPressEnter} style={{ width: '32px', height: '32px', padding: '4px', textAlign: 'center' }}></Input>&nbsp;&nbsp;页
            </span>
          </div>)
  }
}

export default UserRegister;