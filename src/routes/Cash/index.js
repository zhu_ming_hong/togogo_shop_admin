import React from 'react'
import CustomBreadcrumb from '../../components/CustomBreadcrumb/index'
import {Table, Button, Input, DatePicker} from 'antd';
import {login} from "../../api/login";
import './index.css'
const { RangePicker } = DatePicker;
// const { Search } = Input;
const InputGroup = Input.Group;
const columns = [
    {
        title: '序号',
        dataIndex: 'name',
        width: 100,
        align: 'center'
    },
    {
        title: '订单编号',
        dataIndex: 'name',
        width: 200,
        align: 'center'
    },
    {
        title: '购物人',
        dataIndex: 'address',
        width: 200,
        align: 'center'
    },
    {
        title: '订单金额',
        dataIndex: 'name',
        width: 200,
        align: 'center'
    },
    {
        title: '日期',
        dataIndex: 'age',
        width: 200,
        align: 'center'
    }
];

const data = [];
for (let i = 0; i < 46; i++) {
    data.push({
        key: i,
        name: `Edward King ${i}`,
        age: 32,
        address: `London, Park Lane no. ${i}`,
    });
}

class Shop extends React.Component {
    state = {
        selectedRowKeys: [], // Check here to configure the default column
        loading: false,
    };

    start = () => {
        this.setState({loading: true});
        // ajax request after empty completing
        setTimeout(() => {
            this.setState({
                selectedRowKeys: [],
                loading: false,
            });
        }, 1000);
    };

    onSelectChange = selectedRowKeys => {
        // console.log('selectedRowKeys changed: ', selectedRowKeys);
        this.setState({selectedRowKeys});
    };

    componentDidMount() {
        login({id: 1}).then(() => {

        })
    }

    render() {
        const {loading, selectedRowKeys} = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };
        const hasSelected = selectedRowKeys.length > 0;
        return (
            <div>
                <CustomBreadcrumb arr={['商品']}/>
                <div style={{marginBottom: 16}}>
                    <InputGroup compact style={{ borderBottom: '16px' }}>
                      <Button type="primary" onClick={this.start} disabled={!hasSelected} loading={loading}>
                          删除
                      </Button>
                      <Input placeholder="请输入购物人名称"  style={{ width: 120,borderRight: '1px solid rgb(217,217,217)', borderTopRightRadius: '4px', borderBottomRightRadius: '4px' }}
                      />
                      <span className="changeDate">请输入日期:</span>
                      <RangePicker/>
                      <Button type="primary" shape="circle" icon="search" />
                    </InputGroup>
                    
                </div>
                <Table rowSelection={rowSelection} columns={columns} dataSource={data}/>
            </div>
        );
    }
}

export default Shop
