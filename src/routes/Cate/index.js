import React from 'react'
import CustomBreadcrumb from '../../components/CustomBreadcrumb/index'
import {Table, Button, Input, Modal, Form, Row, Col, Select} from 'antd';
import {login} from "../../api/login";
const { Search } = Input;
const {Option} = Select;
const columns = [
    {
        title: '序号',
        dataIndex: 'name',
        width: 100,
        align: 'center'
    },
    {
        title: '订单编号',
        dataIndex: 'name',
        width: 200,
        align: 'center'
    },
    {
        title: '购物人',
        dataIndex: 'address',
        width: 200,
        align: 'center'
    },
    {
        title: '订单金额',
        dataIndex: 'name',
        width: 200,
        align: 'center'
    },
    {
        title: '日期',
        dataIndex: 'age',
        width: 200,
        align: 'center'
    }
];

const data = [];
for (let i = 0; i < 46; i++) {
    data.push({
        key: i,
        name: `Edward King ${i}`,
        age: 32,
        address: `London, Park Lane no. ${i}`,
    });
}

class Cate extends React.Component {
    state = {
        selectedRowKeys: [], // Check here to configure the default column
        loading: false,
        modal2Visible:false
    };

    start = () => {
        this.setState({loading: true});
        // ajax request after empty completing
        setTimeout(() => {
            this.setState({
                selectedRowKeys: [],
                loading: false,
            });
        }, 1000);
    };

    onSelectChange = selectedRowKeys => {
        // console.log('selectedRowKeys changed: ', selectedRowKeys);
        this.setState({selectedRowKeys});
    };

    componentDidMount() {
        login({id: 1}).then(() => {

        })
    }
    setModal2Visible = () =>{
        this.setState({
            modal2Visible:false
        })
    }
    /**
     * 添加分类
     */
    add = () =>{
        this.setState({modal2Visible:true})
    }
    render() {
        const {loading, selectedRowKeys} = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };
        const hasSelected = selectedRowKeys.length > 0;
        const {getFieldDecorator} = this.props.form;
        return (
            <div>
                <CustomBreadcrumb arr={['商品分类']}/>
                <div style={{marginBottom: 16}}>
                    <Button type="primary" onClick={this.start} disabled={!hasSelected} loading={loading}>
                        删除
                    </Button>
                    <span style={{marginLeft: 8}}></span>
                    <Search placeholder="请输入用户名"  style={{ width: 300 }}  />
                    <Button type="primary" style={{float: 'right'}} onClick={() => this.add()}>
                        添加
                    </Button>
                </div>
                <Modal
                    title="添加分类"
                    centered
                    width={1000}
                    visible={this.state.modal2Visible}
                    onOk={() => this.handleSubmit()}
                    onCancel={() => this.setModal2Visible()}
                >
                    <Form>
                        <Row>
                            <Col span={10}>
                                <Form.Item label="分类名称">
                                    {getFieldDecorator('title', {
                                        rules: [{required: true, message: '请输入分类名称!'}],
                                    })(
                                        <Row>
                                            <Col span={20}> <Input placeholder="请输入分类名称"/></Col>
                                        </Row>
                                    )}
                                </Form.Item>
                            </Col>
                            <Col span={10}>
                                <Form.Item label="选择父级分类">
                                    {getFieldDecorator('parent_id', {
                                        rules: [{required: true, message: '选择父级分类!'}],
                                    })(
                                        <Row>
                                            <Col span={20}>
                                                <Select
                                                    placeholder="选择分类"
                                                    onChange={this.handleSelectChange}
                                                    defaultValue="0"
                                                >
                                                    <Option value="0">顶级分类</Option>
                                                    {/*{*/}
                                                    {/*    this.state.MenuPlucks.map((item) => {*/}
                                                    {/*        return ( <Option value={item.id} key={item.id}>{item.name}</Option>)*/}
                                                    {/*    })*/}
                                                    {/*}*/}
                                                </Select>
                                            </Col>
                                        </Row>
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form>
                </Modal>
                <Table rowSelection={rowSelection} columns={columns} dataSource={data}/>
            </div>
        );
    }
}

const CateFrom = Form.create()(Cate);
export default CateFrom
