import React from 'react'
import CustomBreadcrumb from '../../components/CustomBreadcrumb/index'
import {
    Table,
    Button,
    Input,
    Modal,
    Form,
    Select,
    Col,
    Row,
    message,
    Popconfirm,
    DatePicker
} from 'antd';
import {getRoles} from "../../api/role";
import {getPermission,addPermission,delPermission} from "../../api/permissions";
import './index.css';

const {Search,TextArea} = Input;
const {Option} = Select;
const defaultCheckedList = [];
const { RangePicker } = DatePicker;
const InputGroup = Input.Group;


function callback(key) {
    console.log(key);
}

class Permissons extends React.Component {
    state = {
        selectedRowKeys: [], // Check here to configure the default column
        loading: false,
        modal2Visible: false,
        autoExpandParent: false,
        selectedKeys: [],
        data: [],
        groupPlucks: [],
        plainOptions: [],
        checkedList: defaultCheckedList,
        indeterminate: false,
        checkAll: false,
        allPer:[],
        roleList:[]
    };
    onCheck = checkedKeys => {
        console.log('onCheck', checkedKeys);
        this.setState({checkedKeys});
    };
    onSelect = (selectedKeys, info) => {
        console.log('onSelect', info);
        this.setState({selectedKeys});
    }
    start = () => {
        this.setState({loading: true});
        // ajax request after empty completing
        setTimeout(() => {
            this.setState({
                selectedRowKeys: [],
                loading: false,
            });
        }, 1000);
    };
    onSelectChange = selectedRowKeys => {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        this.setState({selectedRowKeys});
    };
    componentDidMount() {
        this._getPermission()
    }
    /**
     * 获取角色列表
     */
    setModal2Visible(modal2Visible) {
        this.setState({modal2Visible});
    }

    /**
     * 添加权限
     */
    _addPermission(data) {
        addPermission(data).then((res) => {
            if(res.code < 400) {
                message.success('添加成功!')
                this._getPermission()
                this.setState({modal2Visible:false});
            }
        })
    }

    /**
     * 获取所有权限
     *
     */
    _getPermission() {
        getPermission().then((res) => {
            if (res.data) {
                this.setState({
                    data:res.data.data
                })
            }
        })
    }
    /**
     * 调用添加
     * @returns {*}
     */
    add = () => {
        this.setState({modal2Visible: true});

    }

    /**
     * 添加数据
     * @returns {*}
     */
    handleSubmitthis = () => {
        this.props.form.validateFields((err, values) => {
                if (err === null) {
                    this._addPermission(values)
                    this.setState({modal2Visible: false});
                }
            }
        )
    }

    /**
     * 删除
     * @returns {*}
     */
    delConfirm = (id) => {
        delPermission(id).then((res) => {
            if (res.code < 400) {
                message.success('删除成功！')
                this._getPermission()
            }
        })
    }
    render() {
        const columns = [
            {
                title: '序号',
                width: 100,
                align: 'center',
                render: (text, record, index)=>{
                    return (index+1)
                }
            },
            {
                title: '权限名称',
                dataIndex: 'name',
                width: 200,
                align: 'center'
            },
            {
                title: '权限简称',
                dataIndex: 'display_name',
                width: 200,
                align: 'center'
            },
            {
                title: '创建时间',
                dataIndex: 'created_at',
                width: 200,
                align: 'center'
            },
            {
                title: '操作',
                dataIndex: 'address',
                width: 200,
                align: 'center',
                render: (text, record) => (
                    <span>
                <Button style={{marginRight: 10}}>修改</Button>
                 <Popconfirm
                     title="你确认要删除吗?"
                     onConfirm={() => {
                         this.delConfirm(record.id)
                     }}
                     okText="确定"
                     cancelText="取消"
                 >
                        <Button type="danger">删除</Button>
                        </Popconfirm>
             </span>
                ),
            }
        ];
        const {loading, selectedRowKeys} = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };
        const hasSelected = selectedRowKeys.length > 0;
        const {getFieldDecorator} = this.props.form;
        return (
            <div>
                <CustomBreadcrumb arr={['权限列表']}/>
                <div style={{marginBottom: 16}}>
                     <InputGroup compact style={{ borderBottom: '16px' }}>
                      <Button type="primary" onClick={this.start} disabled={!hasSelected} loading={loading}>
                          删除
                      </Button>
                      <Input placeholder="请输入权限名称"  style={{ width: 120, borderRight: '1px solid rgb(217,217,217)', borderTopRightRadius: '4px', borderBottomRightRadius: '4px' }} />
                      <span className="changeDate">请输入日期:</span>
                        <RangePicker/>
                        <Button type="primary" shape="circle" icon="search" />
                     </InputGroup>
                </div>
                <Modal
                    title="添加权限"
                    centered
                    width={1000}
                    visible={this.state.modal2Visible}
                    onOk={() => this.handleSubmitthis()}
                    onCancel={() => this.setModal2Visible(false)}
                >
                    <Form>
                        <Form.Item label="权限名称">
                            {getFieldDecorator('name', {
                                rules: [{required: true, message: '请输权限名称!'}],
                            })(
                                <Row span={8}>
                                    <Col span={8}> <Input placeholder="请输入权限名称"/></Col>
                                </Row>
                            )}
                        </Form.Item>
                        <Form.Item label="权限简称">
                            {getFieldDecorator('display_name', {
                                rules: [{required: true, message: '请输入权限简称!'}],
                            })(
                                <Row span={8}>
                                    <Col span={8}> <Input placeholder="请输入权限简称"/></Col>
                                </Row>
                            )}
                        </Form.Item>
                        <Form.Item label="权限描述">
                            {getFieldDecorator('brief', {
                                rules: [{required: true, message: '请输入权限描述!'}],
                            })(
                                <Row span={8}>
                                    <Col span={20}> <TextArea rows={4} /></Col>
                                </Row>
                            )}
                        </Form.Item>
                    </Form>
                </Modal>
                <Table rowSelection={rowSelection} rowKey='id' columns={columns} dataSource={this.state.data}/>
            </div>
        );
    }
}

const PermissonsFrom = Form.create()(Permissons);
export default PermissonsFrom
