import React from 'react'
import CustomBreadcrumb from '../../components/CustomBreadcrumb/index'
import {Table, Button, Input} from 'antd';
import {login} from "../../api/login";
const { Search } = Input;
const InputGroup = Input.Group;
const columns = [
    {
        title: '序号',
        dataIndex: 'name',
        width: 100,
        align: 'center'
    },
    {
        title: '账户名称',
        dataIndex: 'name',
        width: 200,
        align: 'center'
    },
    {
        title: '剩余金额',
        dataIndex: 'address',
        width: 200,
        align: 'center'
    }
];

const data = [];
for (let i = 0; i < 46; i++) {
    data.push({
        key: i,
        name: `Edward King ${i}`,
        age: 32,
        address: `London, Park Lane no. ${i}`,
    });
}

class Shop extends React.Component {
    state = {
        selectedRowKeys: [], // Check here to configure the default column
        loading: false,
    };

    start = () => {
        this.setState({loading: true});
        // ajax request after empty completing
        setTimeout(() => {
            this.setState({
                selectedRowKeys: [],
                loading: false,
            });
        }, 1000);
    };

    onSelectChange = selectedRowKeys => {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        this.setState({selectedRowKeys});
    };

    componentDidMount() {
        login({id: 1}).then(() => {

        })
    }

    render() {
        const {loading, selectedRowKeys} = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };
        const hasSelected = selectedRowKeys.length > 0;
        return (
            <div>
                <CustomBreadcrumb arr={['商品']}/>
                <div style={{marginBottom: 16}}>
                    <InputGroup compact style={{ marginBottom: '16px' }}>
                      <Button type="primary" onClick={this.start} disabled={!hasSelected} loading={loading}>
                          删除
                      </Button>
                       <Input placeholder="请输入账户名称" style={{ width: 150 }}/>
                       <Button type="primary" shape="circle" icon="search"/>
                    </InputGroup>                 
                </div>
                <Table rowSelection={rowSelection} columns={columns} dataSource={data}/>
            </div>
        );
    }
}

export default Shop
