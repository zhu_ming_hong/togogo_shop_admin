import React from 'react'
import CustomBreadcrumb from '../../components/CustomBreadcrumb/index'
import {Table, Button, Input, Modal, Form, Col, Row, message, Popconfirm, DatePicker, BackTop} from 'antd';
import {addGroup ,getGroup ,editGroup,delGroup} from "../../api/group";
import './index.css';
const { RangePicker } = DatePicker;
const InputGroup = Input.Group;


class Group extends React.Component {
    state = {
        selectedRowKeys: [], // Check here to configure the default column
        loading: false,
        modal1Visible: false,
        data:[],
        page:1,
        size:100,
        title:'添加部门',
        name:'',
        id:''

    };
    setModalVisible=(modal1Visible)=>{
        this.setState({
            modal1Visible,
            name:'',
            id:''
        });
    }
    start = () => {
        this.setState({loading: true});
        setTimeout(() => {
            this.setState({
                selectedRowKeys: [],
                loading: false,
            });
        }, 1000);
    };
    onSelectChange = selectedRowKeys => {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        this.setState({selectedRowKeys});
    };
    componentDidMount() {
        this._getGroup()
    }
    //获取组
    _getGroup(){
        const page = this.state.page
        const size = this.state.size
        getGroup(page,size).then(res=>{
           if(res.code < 400) {
               const list = res.data.data
               if(list.length > 0) {
                   this.setState({
                       data:list
                   })
               }
           }
        })
    }
    selectRow=(index) => {
        console.log(index)
    }
    handleSubmit = (e) =>{
        this.props.form.validateFields((err, values) => {
            console.log(values)
            if (err === null ) {
                const id = this.state.id
                if (id) {
                   // 修改
                    editGroup(values,id).then((res) => {
                        if(res.code < 400) {
                            message.success('修改成功！');
                            this._getGroup()
                            this.setState({ modal1Visible:false });
                        }
                    })
                } else {
                    // 添加
                    addGroup(values).then((res) => {
                        if(res.code < 400) {
                            message.success('添加成功！');
                            this._getGroup()
                            this.setState({ modal1Visible:false });
                        }
                    })
                }
            }
        })
    }

    /**
     * 获取修改详情
     * @returns {*}
     */
    edit = (record) => {
        this.props.form.setFieldsValue({
            group_name: record.name
        })
        this.setState({
            modal1Visible:true,
            title:'修改部门',
            name:record.name,
            id:record.id
        });

    }
    /**
     * 删除组
     * @returns {*}
     */
    delConfirm = (id) => {
        delGroup(id).then((res) => {
                if (res.code < 400) {
                    this._getGroup()
                    message.success('删除成功！');
                } else {
                    message.success('删除失败！');
                }
            }
        )
    }
    onChangeInput = (e) => {
        let val = e.target.value
        this.setState({
            name:val
        })
    }
    render() {
        const columns = [
            {
                key:'1',
                title: '序号',
                dataIndex: 'key',
                width: 100,
                align: 'center',
                render: (text, record, index)=>{
                    return (index+1)
                }
            },
            {
                key:'2',
                title: '部门名称',
                dataIndex: 'name',
                width: 200,
                align: 'center'
            },
            {
                key:'3',
                title: '创建时间',
                dataIndex: 'created_at',
                width: 200,
                align: 'center'
            },
            {
                key:'4',
                title: '操作',
                width: 200,
                align: 'center',
                render: (text, record) => (
                    <span>
                        <Button style={{marginRight: 10}} onClick={() => {
                            this.edit(record)
                        }}>修改</Button>
                        <Popconfirm
                            title="你确认要删除吗?"
                            onConfirm={() => {
                                this.delConfirm(record.id)
                            }}
                            okText="确定"
                            cancelText="取消"
                        >
                        <Button type="danger">删除</Button>
                        </Popconfirm>
                    </span>
                ),
            }
        ];

        const {loading, selectedRowKeys} = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };
        const hasSelected = selectedRowKeys.length > 0;
        const { getFieldDecorator } =  this.props.form;
        return (
            <div>
                <CustomBreadcrumb arr={['部门']}/>
                <div style={{marginBottom: 16}}>                
                    <InputGroup compact style={{ borderBottom: '16px' }}>
                      <Button type="primary" onClick={this.start} disabled={!hasSelected} loading={loading}>
                          删除
                      </Button>
                      <Input placeholder="请输入部门名称"  style={{ width: 140,borderRight: '1px solid rgb(217,217,217)', borderTopRightRadius: '4px', borderBottomRightRadius: '4px' }}  />
                      <span className="changeDate">请输入日期:</span>
                      <RangePicker />
                      <Button type="primary" shape="circle" icon="search" />
                    </InputGroup> 
                </div>
                <Modal
                    title={this.state.title}
                    centered
                    width={500}
                    visible={this.state.modal1Visible}
                    onOk={() => this.handleSubmit()}
                    onCancel={() => this.setModalVisible(false)}
                >
                    <Form >
                        <Form.Item label="部门">
                            {getFieldDecorator('group_name', {
                                rules: [{ required: true, message: '请输入部门!' }],
                            })(
                                <Row span={20}>
                                    <Col span={20}> <Input value={this.state.name} onChange={this.onChangeInput}  placeholder="请输入部门"  /></Col>
                                </Row>
                            )}
                        </Form.Item>
                    </Form>
                </Modal>
                <BackTop visibilityHeight={100} style={{right: 12, bottom: 92}}/>
                <Table rowSelection={rowSelection}  rowKey="id"  columns={columns} dataSource={this.state.data} onRow={(record) => ({
                    onClick: () => {
                        this.selectRow(record);
                    },
                })}/>
            </div>
        );
    }
}
const GroupFrom = Form.create()(Group);
export default GroupFrom
