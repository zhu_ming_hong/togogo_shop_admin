import React from 'react'
import {Button, Input, Row, Col} from 'antd';
import {deletePrice} from "../../api/shop";

class shopPrice extends React.Component {
    state = {
        prices:[],
        ids:[],
        delIds:[],
        delete_price_data:[]
    }

    AddPrice = (e) => {
        var price = {
            id:new Date().getTime(),
            price:'',
            activity_price:'',
            need_number:'',
            max_number:'',
            cost_price:'',
            web_type: 1
        }
        const prices = this.state.prices
        prices.push(price)
        this.setState({
            prices
        })
    }

    componentDidMount() {
        this.setState({
            prices:this.props.priceList
        })
    }

    /**
     * 获取组件的数据
     * @returns {*}
     */
    getPrices() {
       return  this.state.prices
    }

    /**
     * 删除价格
     */
    _deletePrice(id){
       return  deletePrice(id).then((res) => {
            return res
        })
    }
    /**
     * 获取变动id
     * @param e
     */
    getIds(){
        let idData = {
            ids:this.state.ids,
            delIds:this.state.delIds,
            delete_price_data:this.state.delete_price_data
        }
        return idData
    }
    setPriceS = (e) => {
        let prices = this.state.prices
        let val = e.target.value
        const id = e.target.id
        for (let index in prices) {
            if(parseInt(prices[index].id) === parseInt(id)) {
                prices[index].price = val
            }
        }
        let ids2 = this.state.ids
        if(!ids2.includes(id)) {
            ids2.push(id)
        }
        this.setState({
            prices
        })
    }
    setNeedNumber = (e) => {
        let prices = this.state.prices
        let val = e.target.value
        const id = e.target.id
        for (let index in prices) {
            if(parseInt(prices[index].id) === parseInt(id)) {
                prices[index].need_number = val
            }
        }
        let ids = this.state.ids
        if(!ids.includes(id)) {
            ids.push(id)
        }
        this.setState({
            prices
        })
    }
    setMaxNumber = (e) => {
        let prices = this.state.prices
        let val = e.target.value
        const id = e.target.id
        for (let index in prices) {
            if(parseInt(prices[index].id) === parseInt(id)) {
                prices[index].max_number = val
            }
        }
        let ids = this.state.ids
        if(!ids.includes(id)) {
            ids.push(id)
        }
        this.setState({
            prices
        })
    }
    setActivityPrice = (e) => {
        let prices = this.state.prices
        let val = e.target.value
        const id = e.target.id
        for (let index in prices) {
            if(parseInt(prices[index].id) === parseInt(id)) {
                prices[index].activity_price = val
            }
        }
        let ids = this.state.ids
        if(!ids.includes(id)) {
            ids.push(id)
        }
        this.setState({
            prices
        })
    }
    setCostPrice = (e) => {
        let prices = this.state.prices
        let val = e.target.value
        const id = e.target.id
        for (let index in prices) {
            if(parseInt(prices[index].id) === parseInt(id)) {
                prices[index].cost_price = val
            }
        }
        let ids = this.state.ids
        if(!ids.includes(id)) {
            ids.push(id)
        }
        this.setState({
            prices
        })
    }
    delPrice = async (e) => {
        let prices = this.state.prices
        const id = e.target.id
        let deletes = await this._deletePrice(id)
        console.log(deletes)
        if(deletes.code < 400) {
            let delIds = []
            let delete_price_data =[]
            for (let index in prices) {
                if(parseInt(prices[index].id) === parseInt(id)) {
                    if(!prices[index].web_type) {
                        delete_price_data = this.state.delete_price_data
                        delete_price_data.push(id)
                    } else {
                        delIds = this.state.delIds
                        delIds.push(id)
                    }
                    prices.splice(index,1)

                }
            }

            this.setState({
                prices,
                delIds,
                delete_price_data
            })
        }
    }
    render() {
        return (
            <Row >
                <Col span={24} style={{borderBottom: '1px solid rgba(0, 0, 0, 0.15)'}}>
                    <Col span={4} style={{textAlign: 'center', height: '40px', lineHeight: '40px'}}>
                        销售价格
                    </Col>
                    <Col span={4} style={{textAlign: 'center', height: '40px', lineHeight: '40px'}}>
                        起始数量
                    </Col>
                    <Col span={4} style={{textAlign: 'center', height: '40px', lineHeight: '40px'}}>
                        最大数量
                    </Col>
                    <Col span={4} style={{textAlign: 'center', height: '40px', lineHeight: '40px'}}>
                        成本价格
                    </Col>
                    <Col span={4} style={{textAlign: 'center', height: '40px', lineHeight: '40px'}}>
                        活动价格
                    </Col>
                    <Col span={4} style={{textAlign: 'center', height: '40px', lineHeight: '40px'}}>
                        <Button type="primary"
                                onClick={(e) => {
                                    this.AddPrice(e)
                                }}>添加</Button>
                    </Col>
                </Col>
                {
                    this.state.prices.map((item) => {
                        return (
                            <Col key={item.id} span={24} style={{borderBottom: '1px solid rgba(0, 0, 0, 0.15)'}}>
                                <Col span={4} style={{textAlign: 'center', height: '40px', lineHeight: '40px'}}>
                                    <Input id={item.id} defaultValue= {item.price} onBlur={(e)=>{this.setPriceS(e)}}/>
                                </Col>
                                <Col span={4} style={{textAlign: 'center', height: '40px', lineHeight: '40px'}}>
                                    <Input id={item.id} defaultValue={item.need_number} onBlur={(e)=>{this.setNeedNumber(e)}}/>
                                </Col>
                                <Col span={4} style={{textAlign: 'center', height: '40px', lineHeight: '40px'}}>
                                    <Input id={item.id} defaultValue={item.max_number} onBlur={(e)=>{this.setMaxNumber(e)}}/>
                                </Col>
                                <Col span={4} style={{textAlign: 'center', height: '40px', lineHeight: '40px'}}>
                                    <Input id={item.id} defaultValue= {item.cost_price} onBlur={(e)=>{this.setCostPrice(e)}}/>
                                </Col>
                                <Col span={4} style={{textAlign: 'center', height: '40px', lineHeight: '40px'}}>
                                    <Input id={item.id}  defaultValue= {item.activity_price}  onBlur={(e)=>{this.setActivityPrice(e)}}/>
                                </Col>
                                <Col span={4} style={{textAlign: 'center', height: '40px', lineHeight: '40px'}}>
                                    <Button type="danger"
                                            id={item.id}
                                            onClick={(e) => {
                                                this.delPrice(e)
                                            }}>删除</Button>
                                </Col>
                            </Col>
                        )
                    })
                }
            </Row>

        )
    }
}

export default shopPrice
