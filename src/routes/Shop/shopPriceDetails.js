import React from 'react'
import {Button, Row, Col, message} from 'antd';

class shopPriceDetails extends React.Component {
    state ={
        pricesIds:[]
    }
    /**
     * 获取组件的数据
     * @returns {*}
     */
    getPriceIds() {
        return this.state.pricesIds
    }
    delPrice = (e) => {
        const id = e.target.id
        const prices = this.props.prices
        for (let index in prices) {
            if(parseInt(prices[index].id) === parseInt(id)) {
                prices.splice(index,1)
            }
        }
        const pricesIds = this.state.pricesIds
        pricesIds.push(id)
        this.setState({
            pricesIds,
            prices
        })
        message.success('删除成功！')
    }
    render() {
        return (
            <Row >
                <Col span={24} style={{borderBottom: '1px solid rgba(0, 0, 0, 0.15)'}}>
                    <Col span={4} style={{textAlign: 'center', height: '40px', lineHeight: '40px'}}>
                        销售价格
                    </Col>
                    <Col span={4} style={{textAlign: 'center', height: '40px', lineHeight: '40px'}}>
                        起始数量
                    </Col>
                    <Col span={4} style={{textAlign: 'center', height: '40px', lineHeight: '40px'}}>
                        最大数量
                    </Col>
                    <Col span={4} style={{textAlign: 'center', height: '40px', lineHeight: '40px'}}>
                        成本价格
                    </Col>
                    <Col span={4} style={{textAlign: 'center', height: '40px', lineHeight: '40px'}}>
                        活动价格
                    </Col>
                    <Col span={4} style={{textAlign: 'center', height: '40px', lineHeight: '40px'}}>
                       操作
                    </Col>
                </Col>
                {
                    this.props.prices.map((item) => {
                        return (
                            <Col key={item.id} span={24} style={{borderBottom: '1px solid rgba(0, 0, 0, 0.15)'}}>
                                <Col span={4} style={{textAlign: 'center', height: '40px', lineHeight: '40px'}}>
                                    {item.price}
                                </Col>
                                <Col span={4} style={{textAlign: 'center', height: '40px', lineHeight: '40px'}}>
                                   {item.need_number}
                                </Col>
                                <Col span={4} style={{textAlign: 'center', height: '40px', lineHeight: '40px'}}>
                                    {item.max_number}
                                </Col>
                                <Col span={4} style={{textAlign: 'center', height: '40px', lineHeight: '40px'}}>
                                    {item.cost_price}
                                </Col>
                                <Col span={4} style={{textAlign: 'center', height: '40px', lineHeight: '40px'}}>
                                    {item.activity_price}
                                </Col>
                                <Col span={4} style={{textAlign: 'center', height: '40px', lineHeight: '40px'}}>
                                    <Button type="danger"
                                            id={item.id}
                                            onClick={(e) => {
                                                this.delPrice(e)
                                            }}>删除</Button>
                                </Col>
                            </Col>
                        )
                    })
                }
            </Row>

        )
    }
}
export default shopPriceDetails
