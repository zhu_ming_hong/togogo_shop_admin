import React from 'react'
import CustomBreadcrumb from "../../components/CustomBreadcrumb";
import {Row, Col, Table, Button} from 'antd';
import {getShopDetail} from "../../api/shop";
import Ueditor from '../../components/Ueditor'
class shopDetail extends React.Component {
    state = {
        data:[]
    }
    componentDidMount() {
        let id = this.props.match.params.id
        console.log(22)
        console.log(id)
        if (id) {
            getShopDetail(id).then((res) => {
                console.log(res.data.id)
                this.setState({
                    data: res.data
                })
            })
        }

    }
    content = ()=>{

    }
    render() {
        const columns = [
            {title: '商品属性', dataIndex: 'sku_name', key: 'sku_name'},
            {title: '价格', dataIndex: 'original_price', key: 'original_price'},
            {title: '库存', dataIndex: 'stock', key: 'stock'},
            {title: '图片', dataIndex: 'images', key: 'images'},
            {title: '条形码', dataIndex: 'bar_code', key: 'bar_code'},
        ];
        const columnsItem = [
            {title: '销售价格', dataIndex: 'price', key: 'price'},
            {title: '起始数量', dataIndex: 'need_number', key: 'need_number'},
            {title: '最大数量', dataIndex: 'max_number', key: 'max_number'},
            {title: '成本价格', dataIndex: 'cost_price', key: 'cost_price'},
            {title: '活动价格', dataIndex: 'activity_price', key: 'activity_price'},
        ];
        return (
            <div>
                <CustomBreadcrumb arr={['商品详情']}/>
                {
                    this.state.data.id > 0 ?
                        <div>
                            <Row gutter={16}>
                                <Col span={6}>
                                    <Col span={6}>商品名称:</Col>
                                    <Col span={18}>{this.state.data.commodity_name}</Col>
                                </Col>
                                <Col span={6}>
                                    <Col span={6}>商品市场价格:</Col>
                                    <Col span={18}>{this.state.data.market_price}</Col>
                                </Col>
                                <Col span={6}>
                                    <Col span={6}>总库存:</Col>
                                    <Col span={18}>{this.state.data.total_stock}</Col>
                                </Col>
                                <Col span={6}>
                                    <Col span={6}>单位:</Col>
                                    <Col span={18}>{this.state.data.unit}</Col>
                                </Col>
                            </Row>
                            <Row gutter={16} style={{marginTop: '20px'}}>
                                <Col span={6}>
                                    <Col span={6}>商品图片:</Col>
                                    <Col span={18}>
                                        <img width={50} height={50} src={this.state.data.commodity_image_thum}/>
                                    </Col>
                                </Col>
                            </Row>
                            <Row gutter={16} style={{marginTop: '20px', marginBottom: '20px'}}>
                                <Col span={6}>
                                    <Col span={6}>商品列表图:</Col>
                                    <Col span={18}>
                                        {
                                            this.state.data.images.map((item) => {
                                                return (
                                                    <img key={item.id} width={50} height={50} src={item.commodity_image_thum}/>
                                                )
                                            })
                                        }
                                    </Col>
                                </Col>
                            </Row>
                            <Row gutter={16} style={{marginTop: '20px'}}>
                                <Col span={24}>
                                    <Col span={6}>商品简介:</Col>
                                    <Col span={24}>
                                        {this.state.data.brief_introduction}
                                    </Col>
                                </Col>
                            </Row>
                            <Row gutter={16} style={{marginTop: '20px'}}>
                                <Col span={24}>
                                    <Col span={6}>商品详情:</Col>
                                    <Col span={24}>
                                        <Ueditor value={this.state.data.details} content={this.content.bind(this)}></Ueditor>
                                    </Col>
                                </Col>
                            </Row>

                            <Table
                                columns={columns}
                                pagination={false}
                                rowKey="key"
                                defaultExpandAllRows={true}
                                expandedRowRender={record =>
                                    <Table
                                        rowKey="key"
                                        columns={columnsItem}
                                        pagination={false}
                                        dataSource={record.prices}
                                    />}
                                dataSource={this.state.data.commodity_sku}
                            />

                            <Button onClick={()=>{this.props.history.go(-1)}}>返回</Button>
                        </div>
                        : ''
                }

            </div>
        )
    }
}

export default shopDetail
