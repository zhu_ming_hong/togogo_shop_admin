import React from 'react' // React 库
import CustomBreadcrumb from '../../components/CustomBreadcrumb/index'// 传值进入该文件，分条件渲染 Ant_D组件 
import {Table, Input, Select, Button, DatePicker } from 'antd'; // Ant_D 按需加载
import { getgoldList, getPages, getgoldListPages } from "../../api/gold";// http 请求 api
const { Search } = Input; // 估计是用来做 input设置
const { Option } = Select;
const InputGroup = Input.Group;
const { RangePicker } = DatePicker;

// 创建 Gold 模块
class Gold extends React.Component {
  state = {
    // GoldGetPage: 1, //默认每次加载一页
    // GoldGetSize: 20, //默认每次加载十条数据
    start_time: '',
    end_time: '',
    type: 0,
    per_page: 0,
    data:[],
    total: 0
  }
  // React生命周期，调用封装请求
  // componentDidMount() 函数，
  // 该函数是 React组件挂载完毕才开始调用的，
  // 属于系统性回调函数
    componentDidMount() {
      this._getdatalist();
    }
    _getdatalist(){
      const data = {
        page: 1,
        size: 10
      }
      getgoldList(data).then((res) => {
        console.log(res);
          const data = res.data.data;
          console.log('返回数据')
          console.log(res);
          if(data){
            this.setState({
              data: data,
              per_page: res.data.per_page,
              total: data.total
            })
          }
      })
    
    // 闭包调用 Promise
  }
    // 查询调用接口
    getInputValue=()=> {
      var inputValue = document.querySelectorAll('input')[0].value;
      console.log(inputValue);
      let data = {
        keywords: inputValue,
        type: this.state.type,
        page: 1,
        size: 10,
        start_time: this.state.start_time,
        end_time: this.state.end_time
      }
      getPages(data).then(res=>{
        const list = res.data.data
        if(list.length === 0){
          this.setState({
            total: -1
          })
          // let jump = document.getElementById('jumpVessel');
          // jump.style.display = 'none';
        }else{
          this.setState({
            total: list.total
          })
        }
        this.setState({
          data: list
        })
      })
    }
    // select选取
    getTypes=(value)=>{
      if(value === '0'){
        this.setState({
          type: ''
        })
      }else if(value === '1'){
        this.setState({
          type: 1
        })
      }else if(value === '2'){
        this.setState({
          type: 2
        })
      }else if(value === '3'){
        this.setState({
          type: 3
        })
      }
    }
     // 页面跳转逻辑
     changePage = (page) => {
      this.setState({
        current: page,
      }, () => {
        var data = {
          page: this.state.current,
          size: 10
        }
        getgoldListPages(data).then((res) => {
          if(res.code < 400) {
            const list = res.data.data;
            console.log(res);
            if(list) {
                this.setState({
                    list: list,
                    total: res.data.total
                })
            }
        }
      })
      })
    }

    textNone=()=>{
      let firstInput = document.querySelectorAll('input')[0].value;
      if(firstInput === ''){
        const data = {
          page:1,
          size:10
      }
      getgoldList(data).then((res) => {
        console.log(res);
          const data = res.data.data;
          console.log('返回数据')
          console.log(res);
          if(data){
            this.setState({
              data: data,
              per_page: res.data.per_page,
              total: data.total
            })
          }
      })
      }
    }

    render() {
    // ## AntD 数组单个对象所需的 keys
    // const columns = [
    //  {
    //    字段：rowKey:  String. 
    //    说明：'1-n' 从第一个开始，1,2,3,4... 以此类推 ;

    //    字段：title: String. 
    //    说明：每个row 所对应的一个标题 ；

    //    字段：dataIndex:  String.
    //    说明：对应的具体字段 （大概是后台字段）；

    //    字段：width: Number. 
    //    说明：所占宽度 ；

    //    字段：align:  String.
    //    说明：设每一列，包括标题在内的文本对齐方式.
    //   } 
    // ... somthing Object
    // ]

      // 自定义导航节点类别
        const columns = [
          {   
            rowKey: '1',
            title: '支付单号',
            dataIndex: 'internal_payment_sn',
            width: 200,
            align: 'center'
          },
          {   
              rowKey: '2',
              title: '用户id',
              dataIndex: 'user_id',
              width: 200,
              align: 'center'
          },
          {
              rowKey: '3',
              title: '操作时间',
              dataIndex: 'updated_at',
              width: 200,
              align: 'center'
          },
          {   
              rowKey: '4',
              title: '操作记录',
              dataIndex: 'name',
              width: 200,
              align: 'center'
          },
          {   
              rowKey: '5',
              title: '操作金额比例',
              dataIndex: 'proportion',
              width: 200,
              align: 'center',
              render: (text)=>{
                if(text){
                  return `${text}%`
                }else{
                  return text
                }
              }
          },
          {   
              rowKey: '6',
              title: '数量金额',
              dataIndex: 'price',
              width: 200,
              align: 'center'
          },
          {   
              rowKey: '7',
              title: '总金额',
              dataIndex: 'total_price',
              width: 200,
              align: 'center',
              render: (text)=>{
                if(text === null){
                  return '0'
                }else{
                  return text
                }
             }
          }
        ];
        return(<div>
          {/* Ant_D Ui组件 */}
           <CustomBreadcrumb arr={['订单']}/>
                <div>
                  {/* Ant_D Ui组件 */}
                  <InputGroup compact onChange={this.textNone}  style={{ marginBottom: '16px' }}>
                    {/* <Button type="primary">
                          删除
                      </Button> */}
                    <Input placeholder="请输入支付单号  或  用户 id"  onChange={this.textNone}  style={{ width: 210, borderRight: '1px solid rgb(217,217,217)', borderTopRightRadius: '4px', borderBottomRightRadius: '4px' }}
                     />
                    <span style={{ padding: '4px 11px 4px 64px' }}>请输入日期:</span>
                    <RangePicker onChange={(value)=>{
                        // 获取搜索框属性改变时，对应的两个时间
                        var timer = value;
                        let start_time = this.$moment(timer[0]._d).format('YYYY-MM-DD');
                        let end_time = this.$moment(timer[1]._d).format('YYYY-MM-DD');
                        this.setState({
                          start_time: start_time,
                          end_time: end_time
                        })
                    }}/> 
                    <Select defaultValue="0" style={{ width: 120 }} onChange={this.getTypes}>
                      <Option value="0">- 全部 -</Option>
                      <Option value="1">- 分享返利 -</Option>
                      <Option value="2">- 提现 -</Option>
                      <Option value="3">- 转入余额 -</Option>
                    </Select>
                    <Button type="primary" shape="circle" icon="search" onClick={() => this.getInputValue()} />
                  </InputGroup>
                </div>
                {/* Ant_D Ui组件 */}
                {/* 传入请求数据, 列id */}
                <Table rowKey="id" columns={columns} dataSource={this.state.data} pagination={{
                  per_page: this.state.per_page,
                  onChange: this.changePage,
                  total: this.state.total
                }}/>
                {/* <span id="jumpVessel" style={{ fontSize: '14px', color: 'rgb(37,37,37)', float:'right', margin: '0px 18px 20px 0px' }}>
                    前往第&nbsp;&nbsp;<Input id="onPressEnter" onPressEnter={this.onPressEnter} style={{ width: '32px', height: '32px', padding: '4px', textAlign: 'center' }}></Input>&nbsp;&nbsp;页
                </span> */}
                {/* pagination 这个属性, 值为 true的时候，可以显示右下角的分页器 */}
        </div>)
    }
}

export default Gold