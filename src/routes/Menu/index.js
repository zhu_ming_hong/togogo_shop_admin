import React from 'react'
import CustomBreadcrumb from '../../components/CustomBreadcrumb/index'
import {Table, Button, Input, Modal, Form, Select, Col, Row, message, Popconfirm, DatePicker, BackTop} from 'antd';
import {addMenu,getMenu,getMenuPlucks,delMenu} from "../../api/menu";
import './index.css';

// const {Search} = Input;
const {Option} = Select;
const { RangePicker } = DatePicker;
const InputGroup = Input.Group;

function callback(key) {
    console.log(key);
}

/**
 * 获取菜单层级
 */

class Menu extends React.Component {
    state = {
        selectedRowKeys: [], // Check here to configure the default column
        loading: false,
        modal2Visible: false,
        autoExpandParent: false,
        selectedKeys: [],
        data: [],
        page:1,
        size:100,
        MenuPlucks:[] //菜单层级

    };
    onCheck = checkedKeys => {
        console.log('onCheck', checkedKeys);
        this.setState({checkedKeys});
    };
    onSelect = (selectedKeys, info) => {
        console.log('onSelect', info);
        this.setState({selectedKeys});
    }
    start = () => {
        this.setState({loading: true});
        // ajax request after empty completing
        setTimeout(() => {
            this.setState({
                selectedRowKeys: [],
                loading: false,
            });
        }, 1000);
    };
    onSelectChange = selectedRowKeys => {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        this.setState({selectedRowKeys});
    };
    componentDidMount() {
        this._getMenu()
    }

    /**
     * 获取角色列表
     */
    setModal2Visible(modal2Visible) {
        this.setState({modal2Visible});
    }

    /**
     * 调用添加
     * @returns {*}
     */
    add = () => {
        this.props.form.setFieldsValue({
            parent_id:0,
            type: 1
        })
        this._getMenuPlucks()
        this.setState({modal2Visible: true});

    }

    /**
     * 添加数据
     * @returns {*}
     */
    handleSubmit = () => {
        this.props.form.validateFields((err, values) => {
                if (err === null) {
                    this._addMenu(values)
                    this.setState({modal2Visible: false});
                }
            }
        )
    }

    /**
     * 获取菜单
     */
    _getMenu(){
        const page = this.state.page
        const size = this.state.size
        getMenu(page,size).then((res) => {
            if(res.code < 400) {
                this.setState({
                    data:res.data.data
                })
            }
        })
    }
    /**
     * 添加菜单
     * @returns {*}
     */
    _addMenu(data) {
        addMenu(data).then((res) => {
            if(res.code <400) {
                message.success('添加成功')
                this._getMenu()
            }
        })
    }

    /**
     * 获取菜单层级
     * @returns {*}
     */
    _getMenuPlucks(){
        getMenuPlucks().then((res) => {
            if(res.code < 400) {
                const MenuPlucks = res.data
                const MenuPlucksData = []
                for (let index in MenuPlucks) {
                    var menu = {
                        id: index,
                        name: MenuPlucks[index]
                    }
                    MenuPlucksData.push(menu)
                }
                this.setState({
                    MenuPlucks:MenuPlucksData
                })
            }
        })
    }

    /**
     * 改变下拉菜单选项
     * @returns {*}
     */
    handleSelectChange= (val) => {
        this.props.form.setFieldsValue({
            parent_id: val
        })
    }
    /**
     * 删除菜单
     */
    delConfirm = (id) => {
        delMenu(id).then((res) =>{
            if (res.code < 400) {
                this._getMenu()
            }
        })
    }
    /**
     * 选择菜单类型
     * @returns {*}
     */
    handleSelectChangeType = (val) => {
        this.props.form.setFieldsValue({
            type: val
        })
    }
    render() {
        const columns = [
            {
                title: '序号',
                width: 50,
                align: 'center',
                render: (text, record, index) => {
                    return (index + 1)
                }
            },
            {
                title: '菜单名称',
                dataIndex: 'title',
                width: 200,
                align: 'center'
            },
            {
                title: '菜单类型',
                dataIndex: 'type',
                width: 200,
                align: 'center',
                render: (text, record)=>{
                    if(record.type === 1) {
                        return '后台菜单'
                    }else {
                        return '员工菜单'
                    }
                }
            },
            {
                title: '路劲',
                dataIndex: 'path',
                width: 200,
                align: 'center'
            },
            {
                title: '创建时间',
                dataIndex: 'created_at',
                width: 200,
                align: 'center'
            },
            {
                title: '操作',
                dataIndex: 'address',
                width: 200,
                align: 'center',
                render: (text, record) => (
                    <span>
                <Button style={{marginRight: 10}}>修改</Button>
                <Popconfirm
                    title="你确认要删除吗?"
                    onConfirm={() => {
                        this.delConfirm(record.id)
                    }}
                    okText="确定"
                    cancelText="取消"
                >
                        <Button type="danger">删除</Button>
                        </Popconfirm>
             </span>
                ),
            }
        ];

        const {loading, selectedRowKeys} = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };
        const hasSelected = selectedRowKeys.length > 0;
        const {getFieldDecorator} = this.props.form;
        return (
            <div>
                <CustomBreadcrumb arr={['菜单列表']}/>
                <div style={{marginBottom: 16}}>
                    <InputGroup compact style={{ borderBottom: '16px' }}>
                      <Button type="primary" onClick={this.start} disabled={!hasSelected} loading={loading}>
                          删除
                      </Button>
                      {/* 文本搜索框 */}
                      <Input placeholder="请输入菜单名称"  style={{ width: 120, borderRight: '1px solid rgb(217,217,217)', borderTopRightRadius: '4px', borderBottomRightRadius: '4px' }} />
                      <span className="changeDate">请输入日期:</span>
                      <RangePicker />
                      <Select defaultValue="yuangongcaidan" style={{ width: 120 }}>
                        <Option value="yuangongcaidan">员工菜单</Option>
                        <Option value="houtaicaidan">后台菜单</Option>
                      </Select>
                      <Button type="primary" shape="circle" icon="search" />
                    </InputGroup>  
                </div>
                <Modal
                    title="添加菜单"
                    centered
                    width={1000}
                    visible={this.state.modal2Visible}
                    onOk={() => this.handleSubmit()}
                    onCancel={() => this.setModal2Visible(false)}
                >
                    <Form>
                        <Row>
                            <Col span={10}>
                                <Form.Item label="菜单名称">
                                    {getFieldDecorator('title', {
                                        rules: [{required: true, message: '请输入菜单名称!'}],
                                    })(
                                        <Row>
                                            <Col span={20}> <Input placeholder="请输入菜单名称"/></Col>
                                        </Row>
                                    )}
                                </Form.Item>
                            </Col>
                            <Col span={10}>
                                <Form.Item label="选择父级菜单">
                                    {getFieldDecorator('parent_id', {
                                        rules: [{required: true, message: '选择父级菜单!'}],
                                    })(
                                        <Row>
                                            <Col span={20}>
                                                <Select
                                                    placeholder="选择父级菜单"
                                                    onChange={this.handleSelectChange}
                                                    defaultValue="0"
                                                >
                                                    <Option value="0">顶级菜单</Option>
                                                    {
                                                        this.state.MenuPlucks.map((item) => {
                                                            return ( <Option value={item.id} key={item.id}>{item.name}</Option>)
                                                        })
                                                    }
                                                </Select>
                                            </Col>
                                        </Row>
                                    )}
                                </Form.Item>
                            </Col>
                            <Col span={10}>
                            <Form.Item label="路径">
                                {getFieldDecorator('path', {
                                    rules: [{required: true, message: '请输入路径"!'}],
                                })(
                                    <Row>
                                        <Col span={20}> <Input placeholder="请输入路径"/></Col>
                                    </Row>
                                )}
                            </Form.Item>
                            </Col>
                            <Col span={10}>
                            <Form.Item label="图标">
                                {getFieldDecorator('icon', {
                                    rules: [{required: true, message: '请输入图标!'}],
                                })(
                                    <Row>
                                        <Col span={20}> <Input placeholder="请输入图标"/></Col>
                                    </Row>
                                )}
                            </Form.Item>
                            </Col>
                            <Col span={10}>
                            <Form.Item label="选择类型">
                                {getFieldDecorator('type', {
                                    rules: [{required: true, message: '选择类型!'}],
                                })(
                                    <Row>
                                        <Col span={20}>
                                            <Select
                                                placeholder="选择类型"
                                                defaultValue='1'
                                                onChange={this.handleSelectChangeType}
                                            >
                                                <Option value="1">后台菜单</Option>
                                                <Option value="2">员工端菜单</Option>
                                            </Select>
                                        </Col>
                                    </Row>
                                )}
                            </Form.Item>
                            </Col>
                        </Row>
                    </Form>
                </Modal>
                <BackTop visibilityHeight={100} style={{right: 12, bottom: 92}}/>
                <Table rowSelection={rowSelection} rowKey='id' columns={columns} dataSource={this.state.data}/>
            </div>
        );
    }
}

const MenuFrom = Form.create()(Menu);
export default MenuFrom
