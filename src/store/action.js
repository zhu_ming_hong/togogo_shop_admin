import { USER } from './actionTypes'
function login(data) {
    return {
        type: USER,
        data
    }
}
export {
    login
}
