import { Table,Select } from 'antd';
import React from 'react'
const { Option } = Select;
const columns = [
    {
        title: '类型',
        dataIndex: 'name',
        align: 'left',
        render:() => {
            return <span>
                  <Select value="颜色" >
                    <Option value="jack">Jack</Option>
                    <Option value="lucy">Lucy</Option>
                    <Option value="Yiminghe">yiminghe</Option>
                  </Select>
            </span>
        },
        width:180
    },
    {
        title: '类型',
        dataIndex: 'name',
        align: 'left',
        render:() => {
            return <span>
                  <Select value="颜色" >
                      <Option value="jack">Jack</Option>
                      <Option value="lucy">Lucy</Option>
                      <Option value="Yiminghe">yiminghe</Option>
                    </Select>
            </span>
        },
        width:180
    },
    {
        title: 'Address',
        dataIndex: 'address',
        align: 'center',
    },
];

const data = [];
for (let i = 0; i < 2; i++) {
    data.push({
        key: i,
        name: `Edward King ${i}`,
        age: 32,
        address: `London, Park Lane no. ${i}`,
    });
}

class ShopTab extends React.Component {
    state = {
        selectedRowKeys: [], // Check here to configure the default column
    };

    render() {
        return <Table columns={columns} bordered  showHeader={false} dataSource={data} />;
    }
}

export default  ShopTab
