
import React from 'react'
import ReactQuill, { Quill } from 'react-quill';
// import { ImageDrop } from 'quill-image-drop-module';
import 'react-quill/dist/quill.snow.css';

// 在quiil中注册quill-image-drop-module
// Quill.register('modules/imageDrop', ImageDrop);
class Ueditor extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            'value':props.value,
        };
        this.editor=null;


    }
    componentDidMount() {
        const textbox = this.refs.textarea;
        const toolbarOptions = [
            ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
            ['blockquote', 'code-block'],
            [{ 'list': 'ordered'}, { 'list': 'bullet' }],
            [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
            [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
            [{ 'direction': 'rtl' }],                         // text direction
            [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
            [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
            [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
            [{ 'align': [] }],
            [ 'image'],
            ['clean']                                         // remove formatting button
        ];
        const options = {
            debug: 'warn',
            modules: {
                toolbar: toolbarOptions
            },
            placeholder: '请输入文本...',
            readOnly: false,
            theme: 'snow'
        };
        const editor =this.editor= new Quill(textbox,options);
        const {value}=this.state;
        if (value) editor.clipboard.dangerouslyPasteHTML(value);
        editor.on('text-change', this.handleChange.bind(this));
    }
    handleChange () {
        let {value}=this.state;
        value = this.editor.root.innerHTML;
        this.setState({value});
        this.props.content(value)
    }
    render() {
        return(
            <div ref="textarea" style={{height:'150px'}}></div>
        )
    }
}

export default Ueditor;
