import React from 'react'
import CustomMenu from "../CustomMenu/index";
// import {isAuthenticated} from '../../utils/Session'
const menus = [
    {
        title: '首页',
        icon: 'home',
        key: '/home'
    },
    {
        title: '用户管理',
        icon: 'laptop',
        key: '/user'
    },
    {
        title: '商品分类',
        icon: 'laptop',
        key: '/cate'
    },
    {
        title: '商品管理',
        icon: 'laptop',
        key: '/shop'
    },
    {
        title: '订单管理',
        icon: 'laptop',
        key: '/order'
    },
    {
        title: '财务管理',
        icon: 'laptop',
        key: '/cw',
        subs: [
            {
                title: '充值管理',
                icon: 'laptop',
                key: '/recharge'
            },
            {
                title: '日常现金',
                icon: 'laptop',
                key: '/cash'
            },
            {
                title: '剩余充值',
                icon: 'laptop',
                key: '/surplus'
            },
            {
                title: '金币管理',
                icon: 'laptop',
                key: '/gold'
            },
            {
                title: '赊账金',
                icon: 'laptop',
                key: '/finance/loan'
            }
        ]
    },
    {
        title: '权限管理',
        icon: 'laptop',
        key: '/wx',
        subs: [
            {
                title: '部门管理',
                icon: 'laptop',
                key: '/group'
            },
            {
                title: '人员管理',
                icon: 'laptop',
                key: '/person'
            },{
                title: '角色管理',
                icon: 'laptop',
                key: '/role'
            },
            {
                title: '权限列表',
                icon: 'laptop',
                key: '/permissions'
            },
            {
                title: '菜单列表',
                icon: 'laptop',
                key: '/menu'
            }
        ]
    },
    {
      title: '审核管理',
      icon: 'laptop',
      key: '/sh',
      subs: [
          {
              title: '赊账金管理',
              icon: 'laptop',
              key: '/examine/loan'
          },
          {
              title: '金币提现',
              icon: 'laptop',
              key: '/examine/gold'
          }
      ]
    }
]

class SiderNav extends React.Component { 
    state={
        menus:[]
    }
    componentDidMount() {
        this._getMenu()
    }
    _getMenu(){
      let menu = JSON.parse(localStorage.getItem("menu"))
      console.log('看看本地存储')
      console.log(menu);
      const menuList = []
        for (let index in menu) {
            if(menu[index].type === 1){
                let val = {
                    title:menu[index].title,
                    icon:menu[index].icon,
                    key:menu[index].path
                }
                if(menu[index].subs.length>0){
                    val.subs = []
                    for (let index1 in menu[index].subs) {
                        let son = {
                            title:menu[index].subs[index1].title,
                            icon:menu[index].subs[index1].icon,
                            key:menu[index].subs[index1].path
                        }
                        val.subs.push(son)
                    }
                }
                menuList.push(val)
            }
        }
        this.setState({
            menus:menuList
        })
    }

    render() {
        return (
            <div style={{height: '100vh', overflowY: 'scroll'}}>
                <div style={styles.logo}></div>
                <CustomMenu menus={this.state.menus}/>
            </div>
        )
    }
}

const styles = {
    logo: {
        height: '32px',
        background: 'rgba(255, 255, 255, .2)',
        margin: '16px'
    }
}

export default SiderNav
