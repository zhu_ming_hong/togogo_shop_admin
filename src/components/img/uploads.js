import {Upload, Icon, Modal} from 'antd';
import React from 'react'
import './index.css'
import {httpUrl} from '../../utils/config'

function getBase64(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
}

class PicturesWall extends React.Component {
    state = {
        previewVisible: false,
        previewImage: '',
        fileList: [
            // {
            //     uid: '-1',
            //     name: 'xxx.png',
            //     status: 'done',
            //     url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
            // },
        ],
        imagesList: []
    };

    componentDidMount() {
        let fileList = []

        let imagesList = this.props.imagesList
        for (let index in imagesList) {
            let val = {
                uid: index,
                name: 'index.png',
                status: 'done',
                url: imagesList[index]
            }
            fileList.push(val)
        }
        this.setState({
            fileList
        })
    }

    handleCancel = () => this.setState({previewVisible: false});

    handlePreview = async file => {
        if (!file.url && !file.preview) {
            file.preview = await getBase64(file.originFileObj);
        }
        console.log(file)
        this.setState({
            previewImage: file.url || file.preview,
            previewVisible: true,
        });
    };
    images = (e) => {
        this.props.images(e)
    }
    handleChange = ({fileList}) => {
        const imagesList = this.state.imagesList
        for (let index in fileList) {
            if (fileList[index].status === 'done' && fileList[index].response) {
                if (fileList[index].response.code < 400) {
                    let url = fileList[index].response.data.url
                    if (!imagesList.includes(url)) {
                        imagesList.push(url)
                    }
                }
            }
        }
        this.images(imagesList)
        this.setState({fileList, imagesList})
    };

    render() {
        const {previewVisible, previewImage, fileList} = this.state;
        const uploadButton = (
            <div>
                <Icon type="plus"/>
                <div className="ant-upload-text">请添加图片</div>
            </div>
        );
        return (
            <div className="clearfix">
                <Upload
                    name='image'
                    action={httpUrl + "/imageFileObject?prefix=tgshop_commodity"}
                    listType="picture-card"
                    fileList={fileList}
                    onPreview={this.handlePreview}
                    onChange={this.handleChange}
                >
                    {fileList.length >= 6 ? null : uploadButton}
                </Upload>
                <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
                    <img alt="example" style={{width: '100%'}} src={previewImage}/>
                </Modal>
            </div>
        );
    }
}

export default PicturesWall
