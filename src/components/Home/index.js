import React from 'react'
import { HeaderTop, HeaderTitle, HeaderNum} from './css'
const IndexList = (props) =>{
    return (
        <HeaderTop>
            <HeaderTitle>
                {props.title}
            </HeaderTitle>
            <HeaderNum>
                {props.num}
            </HeaderNum>
        </HeaderTop>
    )
}
export default IndexList
