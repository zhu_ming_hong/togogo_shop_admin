import styled from 'styled-components';
const HeaderTop = styled.div`
overflow:hidden
background:#fff;
display: flex;
justify-content: space-around;
align-items: flex-start;
height:148px
margin-bottom:13.6px
border-radius:6px
box-shadow:6px 6px 6px rgb(237, 238, 239)
`
const HeaderTitle = styled.div`
margin-top:10px
font-size:18px
`
const  HeaderNum = styled.div`
margin-top:10px
font-size:20px
font-weight:800
`
export {
    HeaderTop,
    HeaderTitle,
    HeaderNum
}
