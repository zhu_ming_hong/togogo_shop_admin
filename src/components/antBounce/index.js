import React from 'react'
import {Button,Card, Modal, message} from 'antd'

class UiModal extends React.Component{
  constructor(props){
    super(props);

    this.state = {
      modalAddInfoVisible: false, //新增信息Modal的显示属性
      _datas: 'null',
      status: ''
    }

  }
  componentDidMount() {
    let {datas} = this.props;
    // console.log(datas)
    this.setState({
      _datas: datas
    })
    if(datas === '通过'){
      this.setState({
        status: 'default'
      })
    }else if(datas === '拒绝'){
      this.setState({
        status: 'danger'
      })
    }
  }
  //弹出一个弹出框用于新增信息
  openModalAddInfo = (type)=>{
    this.setState({modalAddInfoVisible: true})
  }

  //弹窗 loading页
  _success = (changes) => {
    message.config({
      top: 64
    });
    message.success(`${changes}`, 1);
  };

  render() {
    return(
      <div>
        <span>
          <Button type={this.state.status} onClick={()=>{
            let rejectTxt = '已拒绝请求';
            this._success(rejectTxt);
          }}>{this.state._datas}</Button>
        </span>

        {/*title:弹出框标题  visible:是否可见  onCancel:取消按钮，右上角X号事件*/}
        <Modal  
               style={{ marginTop: '2%' }}
               width='500px'
               closable={true}
               visible={this.state.modalAddInfoVisible}
               onCancel={()=>{
                 this.setState({modalAddInfoVisible: false})
               }}
               onOk={()=>{
                this.setState({modalAddInfoVisible: false})
              }}
        >
          <span>请确认操作</span>
        </Modal>

      </div>
    )
  }

}

export default UiModal;