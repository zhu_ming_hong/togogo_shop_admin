import React from 'react'
import { withRouter, Switch, Redirect } from 'react-router-dom'
import LoadableComponent from '../../utils/LoadableComponent'
import PrivateRoute from '../PrivateRoute'
const Home = LoadableComponent(()=>import('../../routes/Home/index'))  //参数一定要是函数，否则不会懒加载，只会代码拆分
const Shop = LoadableComponent(()=>import('../../routes/Shop/index'))  //参数一定要是函数，否则不会懒加载，只会代码拆分
const User = LoadableComponent(()=>import('../../routes/User/index'))  //参数一定要是函数，否则不会懒加载，只会代码拆分
const Order = LoadableComponent(()=>import('../../routes/Order/index'))  //参数一定要是函数，否则不会懒加载，只会代码拆分
const Recharge = LoadableComponent(()=>import('../../routes/Recharge/index'))  //参数一定要是函数，否则不会懒加载，只会代码拆分
const Cash = LoadableComponent(()=>import('../../routes/Cash/index'))  //参数一定要是函数，否则不会懒加载，只会代码拆分
const Surplus = LoadableComponent(()=>import('../../routes/Surplus/index'))  //参数一定要是函数，否则不会懒加载，只会代码拆分
const Person = LoadableComponent(()=>import('../../routes/Person/index'))  //参数一定要是函数，否则不会懒加载，只会代码拆分
const Role = LoadableComponent(()=>import('../../routes/Role/index'))  //参数一定要是函数，否则不会懒加载，只会代码拆分
const Group = LoadableComponent(()=>import('../../routes/Group/index'))  //参数一定要是函数，否则不会懒加载，只会代码拆分
const Permissions = LoadableComponent(()=>import('../../routes/Permissions/index'))  //参数一定要是函数，否则不会懒加载，只会代码拆分
const Menu = LoadableComponent(()=>import('../../routes/Menu/index'))  //参数一定要是函数，否则不会懒加载，只会代码拆分
const OrderDetail = LoadableComponent(()=>import('../../routes/Order/detail'))  //参数一定要是函数，否则不会懒加载，只会代码拆分
const shopDetail = LoadableComponent(()=>import('../../routes/Shop/shopDetail'))  //参数一定要是函数，否则不会懒加载，只会代码拆分
const Cate = LoadableComponent(()=>import('../../routes/Cate/index'))  //参数一定要是函数，否则不会懒加载，只会代码拆分
const Gold = LoadableComponent(()=>import('../../routes/Gold/index'))  // 金币管理模块
const UserRegister = LoadableComponent(()=>import('../../routes/userRegister/index')) // 用户注册列表
const UserDetail = LoadableComponent(()=>import('../../routes/userRegister/UserDetail'))
const Examine = LoadableComponent(()=>import('../../routes/Examine/index')) // 赊账金管理
const ExamineGold = LoadableComponent(()=>import('../../routes/Examine/gold')) // 金币提现
@withRouter
class ContentMain extends React.Component {
  render () {
    return (
      <div style={{padding: 16, position: 'relative'}}>
        <Switch>
          <PrivateRoute exact path='/home' component={Home}/>
          <PrivateRoute exact path='/shop' component={Shop}/>
          <PrivateRoute exact path='/user' component={User}/>
          <PrivateRoute exact path='/order' component={Order}/>
          <PrivateRoute exact path='/recharge' component={Recharge}/>
          <PrivateRoute exact path='/cash' component={Cash}/>
          <PrivateRoute exact path='/surplus' component={Surplus}/>
          <PrivateRoute exact path='/person' component={Person}/>
          <PrivateRoute exact path='/role' component={Role}/>
          <PrivateRoute exact path='/group' component={Group}/>
          <PrivateRoute exact path='/permissions' component={Permissions}/>
          <PrivateRoute exact path='/menu' component={Menu}/>
          <PrivateRoute exact path='/orderDetail/:id' component={OrderDetail}/>
          <PrivateRoute exact path='/shopDetail/:id' component={shopDetail}/>
          <PrivateRoute exact path='/cate' component={Cate}/>
          <PrivateRoute exact path='/gold' component={Gold}/>
          <PrivateRoute exact path='/examine/loan' component={Examine}/>
          <PrivateRoute exact path='/examine/gold' component={ExamineGold}/>
          <PrivateRoute exact path='/finance/loan' component={UserRegister}/>
          <PrivateRoute exact path='/financeLoan/interestLogs/:id' component={UserDetail}/>
          <Redirect to='/home'/>
        </Switch>
      </div>
    )
  }
}

export default ContentMain
