import axios from 'axios'
import { message } from 'antd';
import { httpUrl } from './config'

var instance = axios.create({
    baseURL: httpUrl,
    timeout: 200000,
});
// instance.defaults.withCredentials = true
// 添加请求拦截器
instance.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    config.headers['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';
    var url = config.url
    var urls=''
    if (url.indexOf('?') != -1) {
        urls += '&rand=' + Math.random()
    } else {
        urls += '?rand=' + Math.random()
    }
    config["url"]= config.url + urls
    return config;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});
// 添加响应拦截器
instance.interceptors.response.use(function (response) {
    // 对响应数据做点什么
    /**
     * code为非20000是抛错 可结合自己业务进行修改
     */
    const res = response.data
    if (res.code > 400 || res.code === 0) {
        message.error(res.message);
        return Promise.reject('error');
    } else {
        return response.data
    }
}, function (error) {
    message.error('响应失败');
    // 对响应错误做点什么
    return Promise.reject(error);
});
export default instance
